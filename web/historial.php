<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$claves = $_SESSION["usuario"]["claves"];

	if (!in_array("0001", $claves) && !in_array("1003", $claves))
	{
		header("Location: ../index.php?e=2");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <link rel="stylesheet" href="../css/foundation-icons/foundation-icons.css">
    <link rel="stylesheet" href="../css/dataTables.foundation.css">
    <style>
		.mid-opacity
		{
			opacity: 0.5 !important;
		}

		.small-opacity
		{
			opacity: 0.25 !important;
		}

		.tiny-note-left { position:absolute;left:10px;top:2px; }
		.tiny-note-right { position:absolute;right:10px;top:2px; }
    </style>
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA <small id="reloj" style="color:white;"></small></a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="index.php">Inicio</a></li>
				<li><a href="bitacora.php">Bitácora</a></li>
				<li class="active"><a href="historial.php">Historial de Revisión</a></li>
				<li><a href="asistencia.php">Asistencia</a></li>
				<li><a id="cerrar-sesion" href="#">SALIR</a></li>
			</ul>

			<ul class="left hide-for-medium-only hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 columns">
				<h1>Historial de Revisión de las Agendas</h1>
			</div>

			<div class="large-12 columns">
				<table id="dt-historial" class="tdisplay compact" style="width: 100%;">
					<thead>
						<th>#</th>
						<th>Nombre</th>
						<th>Agenda</th>
						<th>Fecha / Hora</th>
					</thead>
				</table>
			</div>
		</div>
	</header>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/img/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/jquery.dataTables.min.js"></script>
	<script src="../js/vendor/dataTables.foundation.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		}
  	});</script>

	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		window.onload = function()
		{
			var date = new Date();
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var temporada = date.getFullYear();
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var reloj =
			{
				run : function()
				{
					var date = new Date();
			    	$("#reloj").html(lpad(date.getDate(),2,"0") + "/" + lpad(date.getMonth()+1,2,"0") + "/" + date.getFullYear() + " " + lpad(date.getHours(),2,"0") + ":" + lpad(date.getMinutes(),2,"0") + ":" + lpad(date.getSeconds(),2,"0"));
				},
				interval : window.setInterval(function()
			    {
			    	reloj.run();
			    }, 1000)
			};
			var modal =
			{
				cargando : document.getElementById("cargando-modal")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "";
				  	}
				}, "json");
			};

			(function ()
			{
				$(modal.cargando).foundation("reveal", "open");
				// Correr el reloj.
				reloj.run();

				// Inicializar Datatables
			    $('#dt-historial').dataTable(
			    {
			    	"language":
			    	{
						"url": "../json/datatables.spanish.lang.json"
					},
			        "processing": true,
			        "serverSide": true,
			        "ajax": '../php/scripts/server_processing.php?option=1',
			        "columns":
			        [
			        	{ "className" : "id" },
			        	{ "className" : "" },
			        	{ "className" : "" },
			        	{ "className" : "" }
			        ],
			        "lengthMenu": [ [100, -1], [100, "All"] ],
			        "order": [[ 0, 'desc' ]],
			        "initComplete": function(settings, json)
					{

					}
			    });

				setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
			})();
		};
	</script>
</body>
</html>