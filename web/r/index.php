<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$claves = $_SESSION["usuario"]["claves"];

	if (!in_array("0001", $claves) && !in_array("4001", $claves))
	{
		header("Location: ../../index.php?e=2");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../../favicon.ico">
    <link rel="stylesheet" href="../../css/normalize.css">
    <link rel="stylesheet" href="../../css/foundation.min.css" />
    <link rel="stylesheet" href="../../css/foundation.calendar.css">
    <link rel="stylesheet" href="../../css/foundation-icons/foundation-icons.css">
    <style>
    	[class*="block-grid-"]>li { padding: 0; }
    	.accordion { margin-left: 0; }
    	.accordion .accordion-navigation>.content.active, .accordion dd>.content.active { background: #F3EFEF; }

    	.switch
    	{
			/*top: -45px;
			right: 20px;*/
			margin-bottom: 0;
    	}

		.switch-on
		{
		  	position: absolute;
		  	left: -50px;
		  	top: 10px;
		  	width: 0;
		  	color: white;
		  	font-weight: bold;
		  	font-size: 9px;
		}

		.switch-off
		{
		  	position: absolute;
		  	left: -25px;
		  	top: 10px;
		  	width: 0;
		  	color: black;
		  	font-weight: bold;
		  	font-size: 9px;
		}

		.switch-label
		{
			position:absolute;
			top:28px;
			left:19px;
		}

		.switch-loading
		{
			position: absolute;
			left: calc(50% - 8px);
			top: calc(50% - 8px);
			z-index: 1;
		}

		.mid-opacity
		{
			opacity: 0.5 !important;
		}

		.small-opacity
		{
			opacity: 0.25 !important;
		}

		.tiny-note-left { position:absolute;left:10px;top:2px; }
		.tiny-note-right { position:absolute;right:10px;top:2px; }

		table.panel-extra-info
		{
			width: 100%;
			margin-top: 5px;
		}

		table.panel-extra-info caption
		{
			font-size: 10px;
			text-align: left;
		}

		table.panel-extra-info tr
		{
			border: 1px solid #ddd;
		}

		table.panel-extra-info td:first-child
		{
			width: 115px;
			background-color: #808080;
    		color: #FFF;
		}

		span[data-happening-now]
		{
			position: absolute;
			top: -5px;
			box-shadow: 2px 2px 3px 0px #6d0a0c;
		}
    </style>
    <script src="../../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA <small id="reloj" style="color:white;"></small></a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a id="cerrar-sesion" href="#">SALIR</a></li>
			</ul>

			<ul class="left hide-for-medium-only hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 columns">
				<!-- TABS -->
				<ul class="tabs small-block-grid-2" data-tab role="tablist">
					<li class="tab-title active" role="presentation"><a href="#panel-eventos-dia" role="tab" tabindex="0" aria-selected="true" aria-controls="panel-eventos-dia">Agenda del Día</a></li>
					<li class="tab-title" role="presentation"><a href="#panel-eventos-mes" role="tab" tabindex="1" aria-selected="false" aria-controls="panel-eventos-mes">Calendario</a></li>
				</ul>

				<div class="tabs-content">
					<!-- CONTENIDO DEL TAB DE 'AGENDA DEL DÍA' -->
					<section role="tabpanel" aria-hidden="false" class="content active" id="panel-eventos-dia">
						<div class="row">
							<div class="large-6 medium-6 small-12 columns">
								<h3 id="eventos-dia-fecha"></h3>
							</div>

							<div class="large-3 medium-3 small-12 columns right">
								<input id="descargar-eventos" type="button" class="small button expand" value="Descargar">
							</div>

							<div class="large-12 columns">
								<ul id="eventos-dia" class="accordion hide" data-accordion></ul>
							</div>

							<div id="no-hay-eventos-dia" class="large-12 columns text-center hide">
								<p><em>No hay eventos para este día.</em></p>
							</div>
						</div>
					</section>

					<!-- CONTENIDO DEL TAB DE 'CALENDARIO' -->
					<section role="tabpanel" aria-hidden="false" class="content" id="panel-eventos-mes">
						<div class="row">
							<div class="large-8 medium-8 small-8 columns">
								<ul class="small-block-grid-2">
									<li>
										<select id="calendar-month">
								    		<option value="1">Enero</option>
								    		<option value="2">Febrero</option>
								    		<option value="3">Marzo</option>
								    		<option value="4">Abril</option>
								    		<option value="5">Mayo</option>
								    		<option value="6">Junio</option>
								    		<option value="7">Julio</option>
								    		<option value="8">Agosto</option>
								    		<option value="9">Septiembre</option>
								    		<option value="10">Octubre</option>
								    		<option value="11">Noviembre</option>
								    		<option value="12">Diciembre</option>
								    	</select>
									</li>
									<li>
										<select id="calendar-year">
								    		<option value="2015">2015</option>
								    		<option value="2016">2016</option>
								    	</select>
									</li>
								</ul>
							</div>

							<div class="large-4 medium-4 small-4 columns end">
								<ul class="button-group even-2">
									<li><a id="calendar-back" href="#back" class="tiny button expand">&#10094;</a></li>
									<li><a id="calendar-forward" href="#forward" class="tiny button expand">&#10095;</a></li>
								</ul>
							</div>

							<div class="large-12 columns">
								<ul id="eventos-del-mes" class="calendar">
								    <li class="title">
								    	
								    </li>

								    <li class="day-header">
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Lunes</span>
								        	<span class="show-for-small">Lun</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Martes</span>
								        	<span class="show-for-small">Mar</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Miercoles</span>
								        	<span class="show-for-small">Mie</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Jueves</span>
								        	<span class="show-for-small">Jue</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Viernes</span>
								        	<span class="show-for-small">Vie</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Sábado</span>
								        	<span class="show-for-small">Sab</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Domingo</span>
								        	<span class="show-for-small">Dom</span>
								        </div>
								    </li>
								</ul>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</header>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../../css/img/cargando.gif"></p>
	</div>

	<script src="../../js/vendor/jquery.js"></script>
	<script src="../../js/foundation.min.js"></script>
  	<script src="../../js/foundation/foundation.topbar.js"></script>
  	<script src="../../js/foundation/foundation.reveal.js"></script>
  	<script src="../../js/foundation/foundation.tab.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		},
		tab:
		{
	    },
		accordion:
		{
	      	callback : function (accordion)
	      	{
	        	if ($(event.target).closest(".switch").length > 0)
	        	{
	        		accordion.addClass("active");
	        	}
	      	}
	    }
  	});</script>

	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		function descargarEventos(JSONString)
		{
			var mapForm = document.createElement("form");
			    mapForm.method = "POST"; // or "post" if appropriate
			    mapForm.action = "../../php/api.php";

			var mapInputAccion = document.createElement("input");
			mapInputAccion.type = "hidden";
			mapInputAccion.name = "accion";
			mapInputAccion.value = "descargar-eventos";

			var mapInputEventos = document.createElement("input");
			mapInputEventos.type = "hidden";
			mapInputEventos.name = "eventos";
			mapInputEventos.value = JSONString;

			var mapInputInicio = document.createElement("input");
			mapInputInicio.type = "hidden";
			mapInputInicio.name = "inicio";
			mapInputInicio.value = $("#eventos-dia-fecha").text();

			mapForm.appendChild(mapInputAccion);
			mapForm.appendChild(mapInputEventos);
			mapForm.appendChild(mapInputInicio);

			var map = window.open("", mapForm.target);

			if (map)
			{
			    mapForm.submit();
			    mapForm.reset();
			}
			else
			{
			    alert("Debes de permitir popups para descargar el archivo.");
			}
		}

		function getFoundationCalendar(month, year)
		{
			$(".week").remove(); // Borrame todos los weeks para agregar nuevos.
			// La primera idea saca desde http://stackoverflow.com/questions/2483719/get-weeks-in-month-through-javascript
			var today = new Date();
			var firstOfMonth = new Date(year, month-1, 1);
		    var lastOfMonth = new Date(year, month, 0);
		    var numberOfDays = lastOfMonth.getDate();
		    var firstDayOfMonth = firstOfMonth.getDay();
		    var usedDays = numberOfDays + firstDayOfMonth;
		    var firstWeek = true;

		    var countDaysFromFirst = firstDayOfMonth;
		    var week = document.createElement("li");
		    week.classList.add("week");

		    for (var i = 1; i <= numberOfDays; i++)
		    {
		    	var day = new Date(year, month-1, i);
		    	var dayDate = day.getDate();
		    	var dayMonth = day.getMonth() + 1;
		    	var dayYear = day.getFullYear();

		    	// Para saber si ya acompleto la semana, crear otro li.week.
		    	if ( (countDaysFromFirst-1) % 7 === 0)
		    	{
	    			if (firstWeek)
	    			{
	    				// Al primer week agregarle los días del mes anterior para acompletar la semana.
	    				if (week.childElementCount < 7)
	    				{
	    					var missingDays = 7 - week.childElementCount;
	    					var previousMonth = new Date(year, month-1, 0);
	    					var lastDayOfPreviousMonth = previousMonth.getDate();
	    					var fromDay = lastDayOfPreviousMonth - missingDays;

	    					for (var j = lastDayOfPreviousMonth; j > fromDay; j--)
	    					{
	    						$(week).prepend("<div class='large-1 medium-1 small-1 day previous-month' data-dia='"+j+"' data-mes='"+
									(previousMonth.getMonth()+1)+"' data-temporada='"+previousMonth.getFullYear()+"' data-fecha='" + j + 
		    						(previousMonth.getMonth()+1) + previousMonth.getFullYear() + "'>" + j + "</div>");
	    					};
	    				};

	    				firstWeek = false;
	    			};

		    		week = document.createElement("li");
		    		week.classList.add("week");
		    	};

		    	// Checar si se encuentra el día de hoy en el mes y temporada seleccionada.
		    	if (day.getFullYear() === today.getFullYear() && day.getMonth() === today.getMonth() && day.getDate() === today.getDate())
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day today' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	}
		    	else
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	};

		    	$("ul#eventos-del-mes").append(week);
		    	countDaysFromFirst += 1;
		    };

		    // Al ultimo week agregarle los dias primero del mes siguiente.
		    if (week.childElementCount < 7)
			{
				var missingDays = 7 - week.childElementCount;
				var nextMonth = new Date(year, month, 1);

				for (var j = 1; j <= missingDays; j++)
				{
					$(week).append("<div class='large-1 medium-1 small-1 day next-month' data-dia='"+j+"' data-mes='"+
						(nextMonth.getMonth()+1)+"' data-temporada='"+nextMonth.getFullYear()+"' data-fecha='" + j + 
						(nextMonth.getMonth()+1)+nextMonth.getFullYear()+"'>" + j + "</div>");
				};
			};

			// Traer los eventos y agregarlos al calendario.
			$.post( "../../php/api.php",
			{
				accion : "obtener-eventos-mes",
				mes : month,
				temporada : year
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var resultado = data.resultado;
			  		var numEventosPorCasilla = 2;
			  		
			  		for (i = 0; i < resultado.length; i++)
			  		{
			  			var fechaInicio = resultado[i].fecha_inicio;
			  			var fechaTermino = resultado[i].fecha_termino;

			  			if (fechaInicio === fechaTermino)
			  			{
			  				var dayDiv = $("li.week div[data-fecha='"+resultado[i].fecha_inicio+"']");
			  				if (dayDiv.length === 0) { continue; };
			  				var numEventos = isNaN(parseInt(dayDiv[0].dataset.numEventos)) ? 1 : parseInt(dayDiv[0].dataset.numEventos) + 1;

			  				if (numEventos > 2)
			  				{
			  					var dayDivMasEventos = dayDiv.find("[data-mas-eventos]");
			  					var numEventosMas = (numEventos-numEventosPorCasilla);

			  					if (dayDivMasEventos.length === 0)
			  					{
			  						dayDiv.append("<div class='row'><div class='columns' data-mas-eventos><span class='alert label hide-for-small-only'>"+
			  						numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span></div></div>");
			  					}
			  					else
			  					{
			  						dayDivMasEventos.html("<span class='alert label hide-for-small-only'>"+numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span>");
			  					};
			  				}
			  				else
			  				{
			  					dayDiv.append("<div class='row'><div class='columns'><span class='alert label'>"+
			  					resultado[i].hora_inicio+" <span class='hide-for-small-only'>- "+resultado[i].hora_termino+"</span></span></div></div>");
			  				};

			  				dayDiv[0].dataset.numEventos = numEventos;
			  			}
			  			else
			  			{
			  				// Para eventos de más de un día.
			  				// var diaDif = fechaTermino - fechaInicio;
			  			};
			  		};
			  	};
			}, "json");

			return;
		};

		window.onload = function()
		{
			var date = new Date();
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var temporada = date.getFullYear();
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var reloj =
			{
				run : function()
				{
					var date = new Date();
			    	$("#reloj").html(lpad(date.getDate(),2,"0") + "/" + lpad(date.getMonth()+1,2,"0") + "/" + date.getFullYear() + " " + lpad(date.getHours(),2,"0") + ":" + lpad(date.getMinutes(),2,"0") + ":" + lpad(date.getSeconds(),2,"0"));

			    	// Proceso para revisar si alguno de los eventos mostrados esta ocurriendo en estos momentos..
			    	var relojUnix = Math.floor(Date.now() / 1000); // Transformamos el valor del reloj a unix para hacer la comparacacion mas adelante..
			    	$(".evento-del-dia").each(function()
			    	{
			    		// Revisar si la fecha y hora actual se encuentra dentro del horario de algun evento..
			    		if ( relojUnix >= parseInt(this.dataset.unixInicio) && relojUnix <= parseInt(this.dataset.unixTermino) )
			    		{
			    			// Si ya tiene la etiqueta 'esta-ocurriendo-ahorita', salir de aqui.
			    			if ($(this).find("[data-happening-now]").length > 0)
				    		{
				    			return;
				    		};

				    		// Agregar la etiqueta al panel del evento, para indicar que 'Esta sucediendo en estos momentos'.
			    			$(this).find("a.evento-dia-titulo").append("<span class='alert label right' data-happening-now>El evento está sucediendo en este momento.</span>");
			    		}
			    		else
			    		{
			    			// Si no tiene etiqueta de 'esta-ocurreidno-ahorita', salir de aqui.
			    			if ($(this).find("[data-happening-now]").length === 0)
				    		{
				    			return;
				    		};

				    		// Quita la etiqueta 'esta-ocurriendo-ahorita', el evento ya termino.
			    			$(this).find("[data-happening-now]").remove();
			    		};
			    	});
				},
				interval : window.setInterval(function()
			    {
			    	reloj.run();
			    }, 1000)
			};
			var eventos =
			{
				delDia : document.getElementById("eventos-del-dia"),
				delMes : document.getElementById("eventos-del-mes"),
				obtenerDelDia : function (dia, mes, temporada, verTab)
				{
					//$(modal.cargando).foundation("reveal", "open");
					$("#eventos-dia-fecha").html(lpad(dia,2,"0")+"/"+lpad(mes,2,"0")+"/"+temporada);
					// Cargar los eventos del día.
					$.post( "../../php/api.php",
					{
						accion: "obtener-eventos-dia",
						dia : dia,
						mes : mes,
						temporada : temporada
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
					  		var resultado = data.resultado;
					  		$("#eventos-dia").html("");

					  		if (resultado.length === 0)
					  		{
					  			$("#eventos-dia").addClass("hide");
					  			$("#no-hay-eventos-dia").removeClass("hide");
					  		}
					  		else
					  		{
					  			$("#eventos-dia").removeClass("hide");
					  			$("#no-hay-eventos-dia").addClass("hide");

					  			for (var i = 0; i < resultado.length; i++)
							  	{
							  		var evento = resultado[i];
							  		var convocados = evento.convocados;
							  		var active = "";
							  		var asistencia = evento.asistencia === "1" ? "checked='true'" : "";

							  		var lista_convocados = document.createElement("ul");
						  			lista_convocados.style.listStyleType = "square";
						  			lista_convocados.style.fontSize = "12px";

							  		for (var j = 0; j < convocados.length; j++)
							  		{
							  			lista_convocados.innerHTML += "<li>"+convocados[j].nombre+"</li>";
							  		}

							  		$("#eventos-dia").append("<li class='accordion-navigation evento-del-dia' data-unix-inicio='"+
						  					evento.unix_inicio+"' data-unix-termino='"+evento.unix_termino+"' data-id='"+evento.id_evento+"'>"+
										"<a class=evento-dia-titulo href='#panel"+i+"' style='padding:0;'>"+
											"<table style='width:100%;margin-bottom:0;'>"+
												"<tr>"+
													"<td><strong style=font-size:12px>"+evento.hora_inicio+" - "+evento.hora_termino+"</strong>: <span style='font-size:14px;'>"+evento.titulo+"</span></td>"+
													"<td><div class='switch round right'><div><img src='../../css/img/cargando.gif' class='switch-loading hide'></div><input id='on-off-"+i+"' class='switch-checkbox asistencia-evento' type='checkbox' "+asistencia+"><label for='on-off-"+i+"' class='switch-on-off'><span class='switch-on'>Sí</span><span class='switch-off'>No</span></label><small class='switch-label'>Asistiré</small></div></td>"+
												"</tr>"+
											"</table>"+
										"</a>"+
										"<div id='panel"+i+"' class='content "+active+"' style='position:relative;'>"+
											//"<small class=tiny-note-left><em>Han confirmado asistencia <strong>"+evento.confirmados+"</strong> de <strong>"+evento.convocados+"</strong></em>.</small>"+
											"<small class=tiny-note-right><em>Creado por <strong>"+evento.creador+"</strong></em>.</small>"+
											"<p style='margin:0'>&#151;&nbsp;"+evento.descripcion+"</p>"+
											"<p style='margin:5px 0 5px 0;'>Hora de llegada del Alcalde <b>"+evento.hora_llegada+"</b> y se retira a las <b>"+evento.hora_salida+"</b>.</p>"+
											"<table class='panel-extra-info'>"+
												"<caption>INFORMACION ADICIONAL</caption>"+
												"<tr><td><span style='font-size:12px'><strong>Representante:</strong></td><td class='td-representante'>"+evento.representante+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Responsable:</strong></td><td>"+evento.responsable+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Vestimenta:</strong></td><td>"+evento.vestimenta+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Logística:</strong></td><td>"+evento.logistica+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Convocados:</strong></td><td>"+lista_convocados.outerHTML+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Ubicación:</strong></td><td>"+evento.ubicacion_dir+"</span></td></tr>"+
											"</table>"+
											//"<a target='_blank' title='Da clic para ver la ubicación del evento.' href='http://maps.google.com?q="+evento.ubicacion_lat+","+evento.ubicacion_long+"'>"+
											//"Da clic aquí para verlo en Google Maps.</a>"+
											"<a href='../imprimir-mapa.php?&q="+evento.ubicacion_lat+","+evento.ubicacion_lng+"' style='color:#CB2B2B;text-decoration:underline;font-size:14px;' target='_blank'>Para imprimir el mapa da clic <b>aqui</b>.</a>"+
											"<iframe width='100%' height='250' frameborder='0' style='border:0'"+
											"src='https://www.google.com/maps/embed/v1/place?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4"+
											"&q="+evento.ubicacion_lat+","+evento.ubicacion_lng+"' allowfullscreen>"+
											"</iframe>"+
											//"<img style='width:100%;' src='https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4"+
											//"&markers="+evento.ubicacion_lat+","+evento.ubicacion_lng+"&zoom=14&size=320x100'>"+
										"</div>"+
									"</li>");
							  	};
					  		};

					  		if (verTab)
					  		{
					  			$("[href='#panel-eventos-dia']").click();
					  		};

					  		// Historial de la visualizacion de la agenda.
					  		$.post("../../php/api.php",
							{
								accion: "vio-agenda",
								dia_agenda: lpad(dia,2,"0"),
								mes_agenda: lpad(mes,2,"0"),
								tem_agenda: temporada
							});
					  	};

					  	//$(modal.cargando).foundation("reveal", "close");
					}, "json");
				}
			}
			var calendario =
			{
				atras : document.getElementById("calendar-back"),
				mes : document.getElementById("calendar-month"),
				temporada : document.getElementById("calendar-year"),
				adelante : document.getElementById("calendar-forward")
			};
			var modal =
			{
				cargando : document.getElementById("cargando-modal")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			calendario.mes.onchange = function()
			{
				getFoundationCalendar(this.value, calendario.temporada.value);
			};

			calendario.temporada.onchange = function()
			{
				getFoundationCalendar(calendario.mes.value, this.value);
			};

			calendario.atras.onclick = function()
			{
				if (calendario.mes.selectedIndex === 0)
				{
					if (calendario.temporada.selectedIndex === 0)
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex -= 1;
					}

					calendario.mes.options[calendario.mes.options.length-1].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex-1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			calendario.adelante.onclick = function()
			{
				if (calendario.mes.selectedIndex === (calendario.mes.options.length-1))
				{
					if (calendario.temporada.selectedIndex === (calendario.temporada.options.length-1))
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex += 1;
					}

					calendario.mes.options[0].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex+1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			// Evento para cargar el evento del día seleccionado en el calendario.
			$(eventos.delMes).on("click", "li.week div.day", function()
			{
				eventos.obtenerDelDia(this.dataset.dia, this.dataset.mes, this.dataset.temporada, true);
			});

			//Evento para recargar los eventos del día de hoy.
			$("#descargar-eventos").click(function(event)
			{
				var eventos = [];
				$(".evento-del-dia").each(function()
				{
					eventos.push(this.dataset.id);
				});

				if (eventos.length === 0)
				{
					alert("No hay eventos para este día.");
					return event.preventDefault();
				};

				descargarEventos(JSON.stringify(eventos));
			});

			// Evento para el switch de asistencia.
			$(document).on("click", ".switch-on-off", function()
			{
				// this.classList.add("small-opacity")

				var divSwitch = this.parentNode;
				// divSwitch.querySelector(".switch-checkbox").disabled = true;
				// divSwitch.querySelector(".switch-loading").classList.remove("hide");
				// divSwitch.querySelector(".switch-label").textContent = "Cargando...";

				var asistenciaEvento = divSwitch.querySelector(".switch-checkbox");
				asistenciaEvento.checked = !asistenciaEvento.checked;

				var confirmarAsistencia;
				var accionAsistencia;

				if (asistenciaEvento.checked)
				{
					confirmarAsistencia = confirm("¿Estás seguro de querer CONFIRMAR tu asistencia a éste evento?");
					accionAsistencia = "CONFIRMAR";
				}
				else
				{
					confirmarAsistencia = confirm("¿Estás seguro de querer CANCELAR tu asistencia a éste evento?");
					accionAsistencia = "CANCELAR";
				};
				

				if (confirmarAsistencia)
				{
					$(modal.cargando).foundation("reveal", "open");
					var eventoDelDia = $(this).closest(".evento-del-dia");

					$.post("../../php/api.php",
					{
						accion: "confirmar-asistencia",
						id: eventoDelDia.attr("data-id"),
						asistencia: asistenciaEvento.checked
					}, function (data)
					{
						if (data.status === "OK")
						{
							alert("!Éxito¡ Acabas de " + accionAsistencia + " tu asistencia.");

							// if (asistenciaEvento.checked)
							// {
							// 	eventoDelDia.find(".td-representante").html("El alcalde SI asistirá.");
							// }
							// else
							// {
							// 	var resultado = data.resultado,
							// 	representante = resultado.representante;

							// 	if (representante === "")
							// 	{
							// 		eventoDelDia.find(".td-representante").html("El alcalde NO asistirá.");
							// 	}
							// 	else
							// 	{
							// 		eventoDelDia.find(".td-representante").html("El alcalde NO asistirá. En cambio, ira " + representante + " en su representación.");
							// 	}
							// }
						}
						else
						{
							alert("¡Oh no! Algo sucedió.\nPor favor, intenta confirmar o cancelar tu asistencia de nuevo.");
						};

						setTimeout(function() {
							$(modal.cargando).foundation("reveal", "close");
						}, 1);
					}, "json");
				}
				else
				{
					asistenciaEvento.checked = !asistenciaEvento.checked;
				};
			});

			(function ()
			{
				$(modal.cargando).foundation("reveal", "open");
				// Correr el reloj.
				reloj.run();

				// Obtener Eventos del Día
				eventos.obtenerDelDia(dia, mes, temporada, true);

				//Llamar a la creación del Calendario
				calendario.mes.value = mes;
				calendario.temporada.value = temporada;
				getFoundationCalendar(mes, temporada);

				setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
			})();
		};
	</script>
</body>
</html>