<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
	
	$permiso_concedido = false;
	for ($i = 0; $i < count($permisos); $i++)
	{
		if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1002")
		{
			$permiso_concedido = true;
		}
	}

	if (!$permiso_concedido)
	{
		header("Location: ../index.php?e=2");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <style>input[type=checkbox]{width:18px;height:18px;vertical-align:top;}</style>
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA</a></h1>
			</li>

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<?php
					for ($i = 0; $i < count($permisos); $i++)
					{
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1001") { echo "<li><a href='index.php'>Inicio</a></li>"; }
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1002") { echo "<li><a href='agendas.php'>Agendas</a></li>"; }
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1003") { echo "<li><a href='eventos.php'>Eventos</a></li>"; }
					}
				?>
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-8 medium-8 small-12 columns">
				<h1>Agendas</h1>
				<p class="subheader">En este módulo, podrás crear una <strong>Agenda</strong> que contenerá eventos que podrán ser vistos por la gente que tu eligas.</p>
			</div>

			<div class="large-4 medium-4 small-12 columns">
				<p class="hide-for-small-only"></p>
				<input id="crear-nueva-agenda" class="button small expand" type="button" value="Crear Nueva Agenda">
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			<form id="form-agenda">
				<div class="row">
					<div class="large-12 columns">
						<label for="form-agenda-nombre">Nombre de la Agenda*</label>
						<input id="form-agenda-nombre" name="nombre" type="text" placeholder="Ingresa el nombre de la agenda." required>
					</div>

					<div class="large-12 columns">
						<label for="form-agenda-comentarios">Comentarios</label>
						<textarea id="form-agenda-comentarios" name="comentarios" rows="5" placeholder="Comentarios sobre la agenda."></textarea>
					</div>

					<div id="form-agenda-contenedor-convocados" class="large-12 columns">
						<label for="grupos-convocados">Personas que podrán ver los eventos de esta agenda</label>
					</div>

					<div class="large-6 medium-6 columns">
						<input id="editar-evento" name="editar-evento" type="submit" class="button small expand" value="Editar" disabled>
					</div>

					<div class="large-6 medium-6 columns">
						<input id="crear-nuevo-evento" name="crear-nuevo-evento" type="submit" class="button small expand" value="Crear Nuevo Evento">
					</div>

					<input type="hidden" name="accion" value="accion-evento">
				</div>
			</form>
		</div>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/img/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/jquery.datetimepicker.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		}
  	});</script>
	<script>
		// DateTimePicker initialize
		function initialize_datetimepicker(fechaInicia, fechaTermina)
		{
			jQuery("#fecha-inicia").datetimepicker(
			{
				lang : "es",
				theme : "default",
				inline : true,
				value : fechaInicia,
				format : "Y-m-d H:i",
				//minDate : 0,
				step : 15,
				onChangeDateTime : function(ct, $input)
				{
					
				}
			});

			jQuery("#fecha-termina").datetimepicker(
			{
				lang : "es",
				theme : "default",
				inline : true,
				value : fechaTermina,
				format : "Y-m-d H:i",
				//minDate : 0,
				step : 15,
				onChangeDateTime : function(ct, $input)
				{
					
				}
			});
		};
	</script>
	<script>
		window.onload = function()
		{
			// Variables
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			// Cargar Agendas.
			$("#cargando-modal").foundation("reveal", "open");

			$.post( "../php/api.php",
			{
				accion : "obtener-agendas"
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		
			  	}
			}, "json");

			// Obtener los Grupos de Convocados
			$.post( "../php/api.php",
			{
				accion: "obtener-grupos-convocados",
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var grupos = data.resultado;

			  		for (var i = 0; i < grupos.length; i++)
			  		{
			  			$("#form-agenda-contenedor-convocados").append("<div class=row>"+
			  				"<div id=form-agenda-contenedor-convocados-grupo class='large-12 columns'>"+
		  						"<input id=grupo-convocado-"+grupos[i].id+" class=grupo-convocados type=checkbox data-grupo="+grupos[i].id+">"+
		  						"<label for=grupo-convocado-"+grupos[i].id+"><strong>"+grupos[i].nombre+"</strong></label>"+
			  					"<ul id=contenedor-convocados-"+grupos[i].id+" style='list-style:none;border-bottom:1px solid #DDD;'></ul>"+
			  				"</div>"+
			  			"</div>")
			  		};

			  		$.post( "../php/api.php",
					{
						accion: "obtener-convocados",
					}, function( data )
					{
						var convocados = data.resultado;

						for (var i = 0; i < convocados.length; i++)
			  			{
			  				$("#contenedor-convocados-"+convocados[i].id_grupo).append("<li>"+
		  						"<input id=convocado-"+convocados[i].id+" class=convocado type=checkbox>"+
		  						"<label for=convocado-"+convocados[i].id+">"+convocados[i].nombre+"</label>"+
			  				"</li>")
			  			};
					}, "json");
			  	};
			}, "json");
		};
	</script>
</body>
</html>