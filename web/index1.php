<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <link rel="stylesheet" href="../css/foundation.calendar.css">
    <!-- <link rel="stylesheet" href="../css/responsive-tables.css"> -->
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA</a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<li><a href="#">Calendario</a></li>
				<li><a href="eventos.php">Eventos</a></li>
				<!-- <li class="has-dropdown">
					<a href="eventos.php">Eventos</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 columns">
				<h1>Calendario <small id="reloj"></small></h1>
			</div>

			<div class="large-12 columns">
				<!-- <ul id="ul-agenda" class="accordion" data-accordion></ul>   -->
				<table id="table-agenda" class="">
					<thead>
						<th>#</th>
						<th>Fecha Inicia</th>
						<th>Fecha Termina</th>
						<th>Título</th>
						<th>Descripcion</th>
						<th>Ubicación</th>
					</thead>

					<tbody></tbody>
				</table>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			
		</div>
	</div>

	<div class="row">
		<div class="large-2 medium-2 small-12 columns">
			<input id="calendar-back" class="button expand" type="button" value="Atras">
		</div>

		<div class="large-4 medium-4 small-12 columns">
			<select id="calendar-month">
	    		<option value="1">Enero</option>
	    		<option value="2">Febrero</option>
	    		<option value="3">Marzo</option>
	    		<option value="4">Abril</option>
	    		<option value="5">Mayo</option>
	    		<option value="6">Junio</option>
	    		<option value="7">Julio</option>
	    		<option value="8">Agosto</option>
	    		<option value="9">Septiembre</option>
	    		<option value="10">Octubre</option>
	    		<option value="11">Noviembre</option>
	    		<option value="12">Diciembre</option>
	    	</select>
		</div>

		<div class="large-4 medium-4 small-12 columns">
			<select id="calendar-year">
	    		<option value="2015">2015</option>
	    		<option value="2016">2016</option>
	    	</select>
		</div>

		<div class="large-2 medium-2 small-12 columns">
			<input id="calendar-forward" class="button expand" type="button" value="Adelante">
		</div>

		<div class="large-12 columns">
			<ul id="calendario" class="calendar">
			    <li class="title">
			    	
			    </li>

			    <li class="day-header">
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Lunes</span>
			        	<span class="show-for-small">Lun</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Martes</span>
			        	<span class="show-for-small">Mar</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Miercoles</span>
			        	<span class="show-for-small">Mie</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Jueves</span>
			        	<span class="show-for-small">Jue</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Viernes</span>
			        	<span class="show-for-small">Vie</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Sábado</span>
			        	<span class="show-for-small">Sab</span>
			        </div>
			        <div class="large-1 medium-1 small-1 day">
			        	<span class="show-for-medium-up">Domingo</span>
			        	<span class="show-for-small">Dom</span>
			        </div>
			    </li>

			    <!-- <li class="week">
			        <div class="large-1 day previous-month">4</div>
			        <div class="large-1 day previous-month">5</div>
			        <div class="large-1 day">6</div>
			        <div class="large-1 day today">7</div>
			        <div class="large-1 day">8</div>
			        <div class="large-1 day">9</div>
			        <div class="large-1 day next-month">10</div>
			    </li> -->
			</ul>
		</div>
	</div>

	<div id="eventos-dia-modal" class="reveal-modal" data-reveal aria-labelledby="eventos-dia-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="eventos-dia-modal-titulo">Eventos del día.</h2>
	  	<div class="row">
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/img/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/jquery.datetimepicker.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<!-- <script src="../js/foundation/responsive-tables.js"></script> -->
  	<!-- <script src="../js/foundation/foundation.accordion.js"></script> -->
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		}
  	});</script>

	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		function getFoundationCalendar(month, year)
		{
			$(".week").remove(); // Borrame todos los weeks para agregar nuevos.
			// La primera idea saca desde http://stackoverflow.com/questions/2483719/get-weeks-in-month-through-javascript
			var today = new Date();
			var firstOfMonth = new Date(year, month-1, 1);
		    var lastOfMonth = new Date(year, month, 0);
		    var numberOfDays = lastOfMonth.getDate();
		    var firstDayOfMonth = firstOfMonth.getDay();
		    var usedDays = numberOfDays + firstDayOfMonth;
		    var firstWeek = true;

		    var countDaysFromFirst = firstDayOfMonth;
		    var week = document.createElement("li");
		    week.classList.add("week");

		    for (var i = 1; i <= numberOfDays; i++)
		    {
		    	var day = new Date(year, month-1, i);
		    	var dayDate = day.getDate();
		    	var dayMonth = day.getMonth() + 1;
		    	var dayYear = day.getFullYear();

		    	// Para saber si ya acompleto la semana, crear otro li.week.
		    	if ( (countDaysFromFirst-1) % 7 === 0)
		    	{
	    			if (firstWeek)
	    			{
	    				// Al primer week agregarle los días del mes anterior para acompletar la semana.
	    				if (week.childElementCount < 7)
	    				{
	    					var missingDays = 7 - week.childElementCount;
	    					var previousMonth = new Date(year, month-1, 0);
	    					var lastDayOfPreviousMonth = previousMonth.getDate();
	    					var fromDay = lastDayOfPreviousMonth - missingDays;

	    					for (var j = lastDayOfPreviousMonth; j > fromDay; j--)
	    					{
	    						$(week).prepend("<div class='large-1 medium-1 small-1 day previous-month' data-dia='"+j+"' data-mes='"+
									(previousMonth.getMonth()+1)+"' data-temporada='"+previousMonth.getFullYear()+"' data-fecha='" + j + 
		    						(previousMonth.getMonth()+1) + previousMonth.getFullYear() + "'>" + j + "</div>");
	    					};
	    				};

	    				firstWeek = false;
	    			};

		    		week = document.createElement("li");
		    		week.classList.add("week");
		    	};

		    	// Checar si se encuentra el día de hoy en el mes y temporada seleccionada.
		    	if (day.getFullYear() === today.getFullYear() && day.getMonth() === today.getMonth() && day.getDate() === today.getDate())
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day today' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	}
		    	else
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	};

		    	$("ul#calendario").append(week);
		    	countDaysFromFirst += 1;
		    };

		    // Al ultimo week agregarle los dias primero del mes siguiente.
		    if (week.childElementCount < 7)
			{
				var missingDays = 7 - week.childElementCount;
				var nextMonth = new Date(year, month, 1);

				for (var j = 1; j <= missingDays; j++)
				{
					$(week).append("<div class='large-1 medium-1 small-1 day next-month' data-dia='"+j+"' data-mes='"+
						(nextMonth.getMonth()+1)+"' data-temporada='"+nextMonth.getFullYear()+"' data-fecha='" + j + 
						(nextMonth.getMonth()+1)+nextMonth.getFullYear()+"'>" + j + "</div>");
				};
			};

			// Traer los eventos y agregarlos al calendario.
			$.post( "../php/api.php",
			{
				accion : "obtener-eventos-calendario",
				mes : month,
				temporada : year
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var resultado = data.resultado;
			  		var numEventosPorCasilla = 2;
			  		
			  		for (i = 0; i < resultado.length; i++)
			  		{
			  			var fechaInicia = resultado[i].fecha_inicia;
			  			var fechaTermina = resultado[i].fecha_termina;

			  			if (fechaInicia === fechaTermina)
			  			{
			  				var dayDiv = $("li.week div[data-fecha='"+resultado[i].fecha_inicia+"']");
			  				var numEventos = isNaN(parseInt(dayDiv[0].dataset.numEventos)) ? 1 : parseInt(dayDiv[0].dataset.numEventos) + 1;

			  				if (numEventos > 2)
			  				{
			  					var dayDivMasEventos = dayDiv.find("[data-mas-eventos]");
			  					var numEventosMas = (numEventos-numEventosPorCasilla);

			  					if (dayDivMasEventos.length === 0)
			  					{
			  						dayDiv.append("<div class='row'><div class='columns' data-mas-eventos><span class='alert label hide-for-small-only'>"+
			  						numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span></div></div>");
			  					}
			  					else
			  					{
			  						dayDivMasEventos.html("<span class='alert label hide-for-small-only'>"+numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span>");
			  					};
			  				}
			  				else
			  				{
			  					dayDiv.append("<div class='row'><div class='columns'><span class='alert label'>"+
			  					resultado[i].hora_inicia+" <span class='hide-for-small-only'>- "+resultado[i].hora_termina+"</span></span></div></div>");
			  				};

			  				dayDiv[0].dataset.numEventos = numEventos;
			  			}
			  			else
			  			{
			  				// Para eventos de más de un día.
			  				// var diaDif = fechaTermina - fechaInicia;
			  			};
			  		};
			  	};
			}, "json");

			return;
		};

		window.onload = function()
		{
			var date = new Date();
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var temporada = date.getFullYear();
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var reloj =
			{
				run : function()
				{
					var date = new Date();
			    	$("#reloj").html(lpad(date.getDate(),2,"0") + "/" + lpad(date.getMonth()+1,2,"0") + "/" + date.getFullYear() + " " + lpad(date.getHours(),2,"0") + ":" + lpad(date.getMinutes(),2,"0") + ":" + lpad(date.getSeconds(),2,"0"));
				},
				interval : window.setInterval(function()
			    {
			    	reloj.run();
			    }, 1000)
			};
			var agenda = document.getElementById("table-agenda");
			var calendario =
			{
				atras : document.getElementById("calendar-back"),
				mes : document.getElementById("calendar-month"),
				temporada : document.getElementById("calendar-year"),
				adelante : document.getElementById("calendar-forward")
			};
			var modal =
			{
				evento : document.getElementById("eventos-dia-modal"),
				cargando : document.getElementById("cargando-modal")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			calendario.mes.onchange = function()
			{
				getFoundationCalendar(this.value, calendario.temporada.value);
			};

			calendario.temporada.onchange = function()
			{
				getFoundationCalendar(calendario.mes.value, this.value);
			};

			calendario.atras.onclick = function()
			{
				if (calendario.mes.selectedIndex === 0)
				{
					if (calendario.temporada.selectedIndex === 0)
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex -= 1;
					}

					calendario.mes.options[calendario.mes.options.length-1].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex-1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			calendario.adelante.onclick = function()
			{
				if (calendario.mes.selectedIndex === (calendario.mes.options.length-1))
				{
					if (calendario.temporada.selectedIndex === (calendario.temporada.options.length-1))
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex += 1;
					}

					calendario.mes.options[0].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex+1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			$("ul#calendario").on("click", "li.week div.day", function()
			{
				$(modal.cargando).foundation("reveal", "open");

				$.post( "../php/api.php",
				{
					accion: "obtener-eventos-dia",
					dia : this.dataset.dia,
					mes : this.dataset.mes,
					temporada : this.dataset.temporada
				}, function( data )
				{
					//$(modal.cargando).foundation("reveal", "close");

				  	if ( data.status === "OK" )
				  	{
				  		var resultado = data.resultado;
				  		var modalEventoRow = $(modal.evento).find("div.row");
				  		modalEventoRow.html("");

				  		for (var i = 0; i < resultado.length; i++)
				  		{
				  			modalEventoRow.append("<div class='large-12 columns panel'>"+
				  				// "<h5 class='text-right'>"+(i+1)+"</h5>"+
				  				// "<div class='large-1 medium-1 small-12 columns show-for-large-only'>"+
			  					// 	"<label>#</label><span>"+(i+1)+"</span></div>"+
			  					"<div class='large-2 medium-2 small-12 columns'>"+
			  						"<label>Fecha Inicia</label><strong>"+resultado[i].fecha_inicia+"</strong></div>"+
			  					"<div class='large-2 medium-2 small-12 columns'>"+
			  						"<label>Fecha Termina</label><strong>"+resultado[i].fecha_termina+"</strong></div>"+
			  					"<div class='large-3 medium-3 small-12 columns'>"+
			  						"<label>Título</label><strong>"+resultado[i].titulo+"</strong></div>"+
			  					"<div class='large-3 medium-3 small-12 columns'>"+
			  						"<label>Descripcion</label><strong>"+resultado[i].descripcion+"</strong></div>"+
			  					"<div class='large-2 medium-2 small-12 columns'>"+
			  						"<label>Ubicación</label><a target='_blank' title='Da clic para ver la ubicación del evento.' href='http://maps.google.com?q="+
			  							resultado[i].ubicacion_lat+","+resultado[i].ubicacion_long+"'><img src='../css/img/map-32.png'></a></div>"+
			  				"</div></div>");
				  		};

				  		$(modal.evento).foundation("reveal", "open");
				  	}
				}, "json");
			});

			// Correr el reloj.
			reloj.run();

			$.post( "../php/api.php",
			{
				accion: "obtener-eventos-dia",
				dia : dia,
				mes : mes,
				temporada : temporada
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var resultado = data.resultado;
			  		var agendaTbody = $(agenda).find("tbody");
			  		agendaTbody.html("");

			  		if (resultado.length === 0)
			  		{
			  			// $(agenda).html("<li>No se encontraron resultados.</li>");//.html("<li>No se encontraron resultados.</li>");
			  			agendaTbody.append("<tr><td colspan='6'>No se encontraron resultados.</td></tr>");
			  		}
			  		else
			  		{
			  			for (var i = 0; i < resultado.length; i++)
					  	{
					  		// $(agenda).append("<li class='accordion-navigation'><a href='#panel"+(i+1)+"a'>"+resultado[i].titulo+"</a>"+
					  		// 	"<label>Fecha Inicia</label><strong>"+resultado[i].fecha_inicia+"</strong>"+
					  		// 	"<label>Fecha Termina</label><strong>"+resultado[i].fecha_termina+"</strong>"+
					  		// 	"<label>Título</label><strong>"+resultado[i].titulo+"</strong>"+
					  		// 	"<label>Descripcion</label><strong>"+resultado[i].descripcion+"</strong>"+
					  		// 	"<label>Ubicación</label><a target='_blank' href='http://maps.google.com?q="+
			  				// 		resultado[i].ubicacion_lat+","+resultado[i].ubicacion_long+"'><img src='../css/img/map-32.png'></a>"+
			  				// "</li>");

					  		agendaTbody.append("<tr>"+
					  			"<td>"+(i+1)+"</td>"+
					  			"<td>"+resultado[i].fecha_inicia+"</td>"+
					  			"<td>"+resultado[i].fecha_termina+"</td>"+
					  			"<td>"+resultado[i].titulo+"</td>"+
					  			"<td>"+resultado[i].descripcion+"</td>"+
					  			"<td>"+
					  				"<a target='_blank' href='http://maps.google.com?q="+resultado[i].ubicacion_lat+","+resultado[i].ubicacion_long+"'>"+
					  					"<img src='../css/img/map-32.png'>"+
					  				"</a>"+
					  			"</td>"+
					  		"</tr>");
					  	};
			  		};
			  	};
			}, "json");

			//Llamar a la creación del Calendario
			calendario.mes.value = mes;
			calendario.temporada.value = temporada;
			getFoundationCalendar(mes, temporada);
		};
	</script>
</body>
</html>