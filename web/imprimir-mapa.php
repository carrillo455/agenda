<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) || !isset($_GET["q"]) )
	{
		header("Location: ../");
		exit;
	}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="shortcut icon" href="../favicon.ico">
    <title>Agenda SIA | Mapa del Evento</title>
    <style>
    	html,
    	body {
			width: 100%;
			height: 100%;
			margin: 0;
			padding: 0;
    	}

    	body {
    		text-align: center;
    	}

    	iframe {
    		box-sizing: border-box;
    	}

    	#imprimir {
    		margin: 20px;
    		height: 35px;
    	}

    	@media print {
			#imprimir {
				display: none;
			}

			@page {
				margin: 8mm;
			}
    	}
    </style>
</head>
<body>
	<input id="imprimir" type="button" value="IMPRIMIR MAPA" onclick="window.print();">
	<img style='width:100%;' src='https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4&markers=<?php echo $_GET["q"]?>&zoom=16&size=700x400'>
</body>
</html>