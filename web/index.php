<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$claves = $_SESSION["usuario"]["claves"];

	if (!in_array("0001", $claves) && !in_array("1001", $claves))
	{
		header("Location: ../index.php?e=2");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <link rel="stylesheet" href="../css/foundation.calendar.css">
    <link rel="stylesheet" href="../css/foundation-icons/foundation-icons.css">
    <link rel="stylesheet" href="../css/google-maps.css">
    <style>
    	[class*="block-grid-"]>li { padding: 0; }
    	.accordion { margin-left: 0; }
    	.accordion .accordion-navigation>.content.active, .accordion dd>.content.active { background: #F3EFEF; }

    	.switch
    	{
			/*top: -45px;
			right: 20px;*/
			margin-bottom: 0;
    	}

		.switch-on
		{
		  	position: absolute;
		  	left: -50px;
		  	top: 10px;
		  	width: 0;
		  	color: white;
		  	font-weight: bold;
		  	font-size: 9px;
		}

		.switch-off
		{
		  	position: absolute;
		  	left: -25px;
		  	top: 10px;
		  	width: 0;
		  	color: black;
		  	font-weight: bold;
		  	font-size: 9px;
		}

		.switch-label
		{
			position:absolute;
			top:28px;
			left:19px;
		}

		.switch-loading
		{
			position: absolute;
			left: calc(50% - 8px);
			top: calc(50% - 8px);
			z-index: 1;
		}

		.mid-opacity
		{
			opacity: 0.5 !important;
		}

		.small-opacity
		{
			opacity: 0.25 !important;
		}

		.tiny-note-left { position:absolute;left:10px;top:2px; }
		.tiny-note-right { position:absolute;right:10px;top:2px; }

		table.panel-extra-info
		{
			width: 100%;
			margin-top: 5px;
		}

		table.panel-extra-info caption
		{
			font-size: 10px;
			text-align: left;
		}

		table.panel-extra-info tr
		{
			border: 1px solid #ddd;
		}

		table.panel-extra-info td:first-child
		{
			width: 115px;
			background-color: #808080;
    		color: #FFF;
		}

		span[data-happening-now]
		{
			position: absolute;
			top: -5px;
			box-shadow: 2px 2px 3px 0px #6d0a0c;
		}
    </style>
    <!-- <link rel="stylesheet" href="../css/responsive-tables.css"> -->
    <script src="../js/vendor/modernizr.js"></script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA <small id="reloj" style="color:white;"></small></a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<?php
					//if (in_array("0001", $claves) || in_array("1001", $claves)) { echo "<li><a href='index.php'>Inicio</a></li>"; }
					//if (in_array("0001", $claves) || in_array("1002", $claves)) { echo "<li><a href='agendas.php'>Agendas</a></li>"; }
					//if (in_array("0001", $claves) || in_array("1003", $claves)) { echo "<li><a href='eventos.php'>Eventos</a></li>"; }
					//if (in_array("0001", $claves) || in_array("1004", $claves)) { echo "<li><a href='convocados.php'>Convocados</a></li>"; }
				?>
				<!-- <li><a href="#">Inicio</a></li>
				<li><a href="eventos.php">Eventos</a></li> -->
				<!-- <li class="has-dropdown">
					<a href="eventos.php">Eventos</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li class="active"><a href="index.php">Inicio</a></li>
				<?php
					if (in_array("0001", $claves) || in_array("1002", $claves)) {
						echo '<li><a href="bitacora.php">Bitácora</a></li>';
					}

					if (in_array("0001", $claves) || in_array("1003", $claves)) {
						echo '<li><a href="historial.php">Historial de Revisión</a></li>';
					}

					if (in_array("0001", $claves) || in_array("1004", $claves)) {
						echo '<li><a href="asistencia.php">Asistencia</a></li>';
					}
				?>
				<li><a id="cerrar-sesion" href="#">SALIR</a></li>
			</ul>

			<ul class="left hide-for-medium-only hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 columns">
				<!-- TABS -->
				<?php
					if (in_array("0001",$claves) || in_array("1005",$claves))
					{
						echo "<ul class=\"tabs small-block-grid-3\" data-tab role=\"tablist\">
							<li class=\"tab-title active\" role=\"presentation\"><a href=\"#panel-eventos-dia\" role=\"tab\" tabindex=\"0\" aria-selected=\"true\" aria-controls=\"panel-eventos-dia\">Agenda</a></li>
							<li class=\"tab-title\" role=\"presentation\"><a href=\"#panel-eventos-mes\" role=\"tab\" tabindex=\"1\" aria-selected=\"false\" aria-controls=\"panel-eventos-mes\">Calendario</a></li>
							<li class=\"tab-title\" role=\"presentation\"><a href=\"#panel-agendar-eventos\" data-load=0 role=\"tab\" tabindex=\"2\" aria-selected=\"false\" aria-controls=\"panel-agendar-eventos\">Eventos</a></li>
						</ul>";
					}
					else
					{
						echo "<ul class=\"tabs small-block-grid-2\" data-tab role=\"tablist\">
							<li class=\"tab-title active\" role=\"presentation\"><a href=\"#panel-eventos-dia\" role=\"tab\" tabindex=\"0\" aria-selected=\"true\" aria-controls=\"panel-eventos-dia\">Agenda del Día</a></li>
							<li class=\"tab-title\" role=\"presentation\"><a href=\"#panel-eventos-mes\" role=\"tab\" tabindex=\"1\" aria-selected=\"false\" aria-controls=\"panel-eventos-mes\">Calendario</a></li>
						</ul>";
					}
				?>

				<div class="tabs-content">
					<!-- CONTENIDO DEL TAB DE 'AGENDA DEL DÍA' -->
					<section role="tabpanel" aria-hidden="false" class="content active" id="panel-eventos-dia">
						<div class="row">
							<div class="large-6 medium-6 small-12 columns">
								<h3 id="eventos-dia-fecha"></h3>
							</div>

							<?php
								if (in_array("0001",$claves) || in_array("1005",$claves))
								{
									echo "<div class=\"large-3 medium-3 small-12 columns right\">
										<input id=\"agendar-evento\" type=\"button\" class=\"small button expand success\" value=\"Agendar Evento\">
									</div>";
								}

								if (in_array("0001",$claves) || in_array("1006",$claves))
								{
									echo "<div class=\"large-3 medium-3 small-12 columns right\">
										<input id=\"descargar-eventos\" type=\"button\" class=\"small button expand\" value=\"Descargar\">
									</div>";
								}
							?>

							<div class="large-12 columns">
								<ul id="eventos-dia" class="accordion hide" data-accordion></ul>
							</div>

							<div id="no-hay-eventos-dia" class="large-12 columns text-center hide">
								<p><em>No hay eventos para este día.</em></p>
							</div>
						</div>
					</section>

					<!-- CONTENIDO DEL TAB DE 'CALENDARIO' -->
					<section role="tabpanel" aria-hidden="false" class="content" id="panel-eventos-mes">
						<div class="row">
							<div class="large-8 medium-8 small-8 columns">
								<ul class="small-block-grid-2">
									<li>
										<select id="calendar-month">
								    		<option value="1">Enero</option>
								    		<option value="2">Febrero</option>
								    		<option value="3">Marzo</option>
								    		<option value="4">Abril</option>
								    		<option value="5">Mayo</option>
								    		<option value="6">Junio</option>
								    		<option value="7">Julio</option>
								    		<option value="8">Agosto</option>
								    		<option value="9">Septiembre</option>
								    		<option value="10">Octubre</option>
								    		<option value="11">Noviembre</option>
								    		<option value="12">Diciembre</option>
								    	</select>
									</li>
									<li>
										<select id="calendar-year">
								    		<option value="2015">2015</option>
								    		<option value="2016">2016</option>
								    	</select>
									</li>
								</ul>
							</div>

							<div class="large-4 medium-4 small-4 columns end">
								<ul class="button-group even-2">
									<li><a id="calendar-back" href="#back" class="tiny button expand">&#10094;</a></li>
									<li><a id="calendar-forward" href="#forward" class="tiny button expand">&#10095;</a></li>
								</ul>
							</div>

							<div class="large-12 columns">
								<ul id="eventos-del-mes" class="calendar">
								    <li class="title">
								    	
								    </li>

								    <li class="day-header">
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Lunes</span>
								        	<span class="show-for-small">Lun</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Martes</span>
								        	<span class="show-for-small">Mar</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Miercoles</span>
								        	<span class="show-for-small">Mie</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Jueves</span>
								        	<span class="show-for-small">Jue</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Viernes</span>
								        	<span class="show-for-small">Vie</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Sábado</span>
								        	<span class="show-for-small">Sab</span>
								        </div>
								        <div class="large-1 medium-1 small-1 day">
								        	<span class="show-for-medium-up">Domingo</span>
								        	<span class="show-for-small">Dom</span>
								        </div>
								    </li>
								</ul>
							</div>
						</div>
					</section>

					<!-- EN CASO DE TENER ACCESSO, EL CONTENIDO DEL TAB DE EVENTOS -->
					<?php
						if (in_array("0001",$claves) || in_array("1005",$claves))
						{
							echo "<section role=\"tabpanel\" aria-hidden=\"false\" class=\"content\" id=\"panel-agendar-eventos\">
								<form id=\"form-evento\" action=\"../php/api.php\" method=\"POST\">
									<div class=\"row\">
										<div class=\"large-12 columns\">
											<label for=\"titulo-evento\">Título*</label>
											<input id=\"titulo-evento\" name=\"titulo\" type=\"text\" placeholder=\"Título del Evento.\" data-title='Título' required>
										</div>

										<div class=\"large-12 columns\">
											<label for=\"descripcion-evento\">Descripción</label>
											<textarea id=\"descripcion-evento\" name=\"descripcion\" rows=\"5\" placeholder=\"Descripción del Evento.\"></textarea>
										</div>

										<div class=\"large-12 columns\">
											<label for=\"responsable-evento\">Responsable</label>
											<input id=\"responsable-evento\" name=\"responsable\" type=\"text\" placeholder=\"Responsable del Evento.\">
										</div>

										<div class=\"large-8 medium-8 small-8 columns\">
											<label for=\"vestimenta-evento\">Vestimenta</label>
											<input id=\"vestimenta-evento\" name=\"vestimenta\" type=\"text\" placeholder=\"Vestimenta para el Evento.\">
										</div>

										<div class=\"large-4 medium-4 small-4 columns\">
											<label for=\"logistica-evento\">Logistica</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"logistica-evento\" name=\"logistica\" type=\"checkbox\"><label for=\"logistica-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-12 medium-12 small-12 columns\">
											<p style='margin:0 0 3px;font-size:14px;'>Convocados</p>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-alcalde-evento\">Alcalde</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-alcalde-evento\" name=\"convocados-alcalde\" type=\"checkbox\"><label for=\"convocados-alcalde-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-cabildo-evento\">Cabildo</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-cabildo-evento\" name=\"convocados-cabildo\" type=\"checkbox\"><label for=\"convocados-cabildo-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-secretarios-evento\">Secretarios</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-secretarios-evento\" name=\"convocados-secretarios\" type=\"checkbox\"><label for=\"convocados-secretarios-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-directores-evento\">Directores</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-directores-evento\" name=\"convocados-directores\" type=\"checkbox\"><label for=\"convocados-directores-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-jefes-evento\">Jefes</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-jefes-evento\" name=\"convocados-jefes\" type=\"checkbox\"><label for=\"convocados-jefes-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-2 medium-2 small-4 columns\">
											<label for=\"convocados-dif-evento\">DIF</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"convocados-dif-evento\" name=\"convocados-dif\" type=\"checkbox\"><label for=\"convocados-dif-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-12 medium-12 small-12 columns\">
											<label for=\"publicado-evento\">¿Lo quieres publicar?</label>
											<div class=\"switch round\" style=\"height: 53px;\"><input id=\"publicado-evento\" name=\"publicado\" type=\"checkbox\"><label for=\"publicado-evento\"><span class=\"switch-on\">Sí</span><span class=\"switch-off\">No</span></label></div>
										</div>

										<div class=\"large-4 medium-4 small-7 columns\">
											<label for=\"fecha-inicio-evento\">Fecha de Inicio*</label>
											<input id=\"fecha-inicio-evento\" name=\"fecha-inicio-evento\" type=\"text\" class=\"fecha\" placeholder=\"dd/mm/aaaa\" autocomplete=\"off\" data-title='Fecha de Inicio' required>
										</div>

										<div class=\"large-2 medium-2 small-5 columns\">
											<label for=\"hora-inicio-evento\">Hora de Inicio*</label>
											<input id=\"hora-inicio-evento\" name=\"hora-inicio-evento\" type=\"text\" class=\"tiempo\" placeholder=\"h24:mm\" autocomplete=\"off\" data-title='Hora de Inicio' required>
										</div>

										<div class=\"large-4 medium-4 small-7 columns\">
											<label for=\"fecha-termino-evento\">Fecha de Termino</label>
											<input id=\"fecha-termino-evento\" name=\"fecha-termino-evento\" type=\"text\" class=\"fecha\" placeholder=\"dd/mm/aaaa\" autocomplete=\"off\">
										</div>

										<div class=\"large-2 medium-2 small-5 columns\">
											<label for=\"hora-termino-evento\">Hora de Termino</label>
											<input id=\"hora-termino-evento\" name=\"hora-termino\" type=\"text\" class=\"tiempo\" placeholder=\"h24:mm\" autocomplete=\"off\">
										</div>

										<div class=\"large-6 medium-6 small-6 columns\">
											<label for=\"hora-llegada-alcalde\">Hora de Llegada</label>
											<input id=\"hora-llegada-alcalde\" name=\"hora-llegada-alcalde\" type=\"text\" class=\"tiempo\" placeholder=\"h24:mm\" autocomplete=\"off\">
										</div>

										<div class=\"large-6 medium-6 small-6 columns\">
											<label for=\"hora-salida-alcalde\">Hora de Salida</label>
											<input id=\"hora-salida-alcalde\" name=\"hora-salida-alcalde\" type=\"text\" class=\"tiempo\" placeholder=\"h24:mm\" autocomplete=\"off\">
										</div>

										<div class=\"large-12 columns\">
											<label for=\"ubicacion-evento\">Ubicación*</label>
											<input id=\"ubicacion-evento\" class=\"controls\" type=\"text\" placeholder=\"Búsqueda\" data-title='Ubicación' data-lat data-lng required>
											<div id=\"map\"></div>
										</div>

										<div id=\"contenedor-crear-evento\" class=\"large-4 medium-4 small-12 columns right\">
											<br>
											<input id=\"crear-evento\" type=\"button\" class=\"small button expand\" value=\"Crear Evento\">
										</div>

										<div id=\"contenedor-guardar-cambios-evento\" class=\"columns hide right\">
											<br>
											<div class='row'>
												<div class=\"large-4 medium-4 small-12 columns right\"><input id=\"guardar-cambios-evento\" type=\"button\" class=\"small button expand\" value=\"Guardar cambios\" data-id></div>
												<div class=\"large-4 medium-4 small-12 columns right\"><input id=\"cancelar-cambios-evento\" type=\"button\" class=\"small alert button expand\" value=\"Cancelar cambios\"></div>
											</div>
										</div>
									</div>
								</form>
							</section>";
						}
					?>
				</div>
			</div>
		</div>
	</header>

	<div id="eventos-dia-modal" class="reveal-modal" data-reveal aria-labelledby="eventos-dia-modal-titulo" aria-hidden="true" role="dialog">
	  	<h2 id="eventos-dia-modal-titulo">Eventos del Día</h2>
	  	<div class="row">
	  	</div>
	  	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/img/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
	<script src="../js/vendor/jquery.mask.min.js"></script>
    <script src="../js/vendor/jquery.datetimepicker.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script src="../js/foundation/foundation.tab.js"></script>
  	<!-- <script src="../js/foundation/foundation.accordion.js"></script> -->
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		},
		tab:
		{
	      	callback : function (tab)
	      	{
	        	if (tab.context.hash === "#panel-agendar-eventos" && tab.context.dataset.load === "0")
	        	{
	        		initMap();
	        		tab.context.dataset.load = 1;
	        	}
	      	}
	    },
		accordion:
		{
	      	callback : function (accordion)
	      	{
	      		// Inicializar la Google Maps API - No es necesario ya que son IFRAMES.
	      		// var panelEventos = $("a[href='#panel-agendar-eventos']");
	      		// if (panelEventos[0].dataset.load === "0")
	      		// {
	      		// 	initMap();
	      		// }

	        	if ($(event.target).closest(".switch").length > 0)
	        	{
	        		// var miSwitch = $(event.target).closest(".switch").find("[type='checkbox']")[0];
	        		// if (miSwitch.disabled)
	        		// {
	        		// 	return false;
	        		// };

	        		// if (miSwitch.checked)
	        		// {
	        		// 	miSwitch.checked = false;
	        		// }
	        		// else
	        		// {
	        		// 	miSwitch.checked = true;
	        		// };

	        		accordion.addClass("active");
	        	}
	      	}
	    }
  	});</script>

	<script>
		function lpad(n, width, z)
		{
			z = z || '0';
			n = n + '';
			return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
		};

		function descargarEventos(JSONString)
		{
			var mapForm = document.createElement("form");
			    mapForm.method = "POST"; // or "post" if appropriate
			    mapForm.action = "../php/api.php";

			var mapInputAccion = document.createElement("input");
			mapInputAccion.type = "hidden";
			mapInputAccion.name = "accion";
			mapInputAccion.value = "descargar-eventos";

			var mapInputEventos = document.createElement("input");
			mapInputEventos.type = "hidden";
			mapInputEventos.name = "eventos";
			mapInputEventos.value = JSONString;

			var mapInputInicio = document.createElement("input");
			mapInputInicio.type = "hidden";
			mapInputInicio.name = "inicio";
			mapInputInicio.value = $("#eventos-dia-fecha").text();

			mapForm.appendChild(mapInputAccion);
			mapForm.appendChild(mapInputEventos);
			mapForm.appendChild(mapInputInicio);

			var map = window.open("", mapForm.target);

			if (map)
			{
			    mapForm.submit();
			    mapForm.reset();
			}
			else
			{
			    alert("Debes de permitir popups para descargar el archivo.");
			}
		}

		function getFoundationCalendar(month, year)
		{
			$(".week").remove(); // Borrame todos los weeks para agregar nuevos.
			// La primera idea saca desde http://stackoverflow.com/questions/2483719/get-weeks-in-month-through-javascript
			var today = new Date();
			var firstOfMonth = new Date(year, month-1, 1);
		    var lastOfMonth = new Date(year, month, 0);
		    var numberOfDays = lastOfMonth.getDate();
		    var firstDayOfMonth = firstOfMonth.getDay();
		    var usedDays = numberOfDays + firstDayOfMonth;
		    var firstWeek = true;

		    var countDaysFromFirst = firstDayOfMonth;
		    var week = document.createElement("li");
		    week.classList.add("week");

		    for (var i = 1; i <= numberOfDays; i++)
		    {
		    	var day = new Date(year, month-1, i);
		    	var dayDate = day.getDate();
		    	var dayMonth = day.getMonth() + 1;
		    	var dayYear = day.getFullYear();

		    	// Para saber si ya acompleto la semana, crear otro li.week.
		    	if ( (countDaysFromFirst-1) % 7 === 0)
		    	{
	    			if (firstWeek)
	    			{
	    				// Al primer week agregarle los días del mes anterior para acompletar la semana.
	    				if (week.childElementCount < 7)
	    				{
	    					var missingDays = 7 - week.childElementCount;
	    					var previousMonth = new Date(year, month-1, 0);
	    					var lastDayOfPreviousMonth = previousMonth.getDate();
	    					var fromDay = lastDayOfPreviousMonth - missingDays;

	    					for (var j = lastDayOfPreviousMonth; j > fromDay; j--)
	    					{
	    						$(week).prepend("<div class='large-1 medium-1 small-1 day previous-month' data-dia='"+j+"' data-mes='"+
									(previousMonth.getMonth()+1)+"' data-temporada='"+previousMonth.getFullYear()+"' data-fecha='" + j + 
		    						(previousMonth.getMonth()+1) + previousMonth.getFullYear() + "'>" + j + "</div>");
	    					};
	    				};

	    				firstWeek = false;
	    			};

		    		week = document.createElement("li");
		    		week.classList.add("week");
		    	};

		    	// Checar si se encuentra el día de hoy en el mes y temporada seleccionada.
		    	if (day.getFullYear() === today.getFullYear() && day.getMonth() === today.getMonth() && day.getDate() === today.getDate())
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day today' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	}
		    	else
		    	{
		    		$(week).append("<div class='large-1 medium-1 small-1 day' data-dia='"+dayDate+"' data-mes='"+dayMonth+"' data-temporada='"+dayYear+"' data-fecha='" +
		    			dayDate + dayMonth + dayYear + "'>" + i + "</div>");
		    	};

		    	$("ul#eventos-del-mes").append(week);
		    	countDaysFromFirst += 1;
		    };

		    // Al ultimo week agregarle los dias primero del mes siguiente.
		    if (week.childElementCount < 7)
			{
				var missingDays = 7 - week.childElementCount;
				var nextMonth = new Date(year, month, 1);

				for (var j = 1; j <= missingDays; j++)
				{
					$(week).append("<div class='large-1 medium-1 small-1 day next-month' data-dia='"+j+"' data-mes='"+
						(nextMonth.getMonth()+1)+"' data-temporada='"+nextMonth.getFullYear()+"' data-fecha='" + j + 
						(nextMonth.getMonth()+1)+nextMonth.getFullYear()+"'>" + j + "</div>");
				};
			};

			// Traer los eventos y agregarlos al calendario.
			$.post( "../php/api.php",
			{
				accion : "obtener-eventos-mes",
				mes : month,
				temporada : year
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var resultado = data.resultado;
			  		var numEventosPorCasilla = 2;
			  		
			  		for (i = 0; i < resultado.length; i++)
			  		{
			  			var fechaInicio = resultado[i].fecha_inicio;
			  			var fechaTermino = resultado[i].fecha_termino;

			  			if (fechaInicio === fechaTermino)
			  			{
			  				var dayDiv = $("li.week div[data-fecha='"+resultado[i].fecha_inicio+"']");
			  				if (dayDiv.length === 0) { continue; };
			  				var numEventos = isNaN(parseInt(dayDiv[0].dataset.numEventos)) ? 1 : parseInt(dayDiv[0].dataset.numEventos) + 1;

			  				if (numEventos > 2)
			  				{
			  					var dayDivMasEventos = dayDiv.find("[data-mas-eventos]");
			  					var numEventosMas = (numEventos-numEventosPorCasilla);

			  					if (dayDivMasEventos.length === 0)
			  					{
			  						dayDiv.append("<div class='row'><div class='columns' data-mas-eventos><span class='alert label hide-for-small-only'>"+
			  						numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span></div></div>");
			  					}
			  					else
			  					{
			  						dayDivMasEventos.html("<span class='alert label hide-for-small-only'>"+numEventosMas+" más...</span><span class='alert label show-for-small-only'>+"+numEventosMas+"</span>");
			  					};
			  				}
			  				else
			  				{
			  					dayDiv.append("<div class='row'><div class='columns'><span class='alert label'>"+
			  					resultado[i].hora_inicio+" <span class='hide-for-small-only'>- "+resultado[i].hora_termino+"</span></span></div></div>");
			  				};

			  				dayDiv[0].dataset.numEventos = numEventos;
			  			}
			  			else
			  			{
			  				// Para eventos de más de un día.
			  				// var diaDif = fechaTermino - fechaInicio;
			  			};
			  		};
			  	};
			}, "json");

			return;
		};

		window.onload = function()
		{
			var date = new Date();
			var dia = date.getDate();
			var mes = date.getMonth() + 1;
			var temporada = date.getFullYear();
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};
			var reloj =
			{
				run : function()
				{
					var date = new Date();
			    	$("#reloj").html(lpad(date.getDate(),2,"0") + "/" + lpad(date.getMonth()+1,2,"0") + "/" + date.getFullYear() + " " + lpad(date.getHours(),2,"0") + ":" + lpad(date.getMinutes(),2,"0") + ":" + lpad(date.getSeconds(),2,"0"));

			    	// Proceso para revisar si alguno de los eventos mostrados esta ocurriendo en estos momentos..
			    	var relojUnix = Math.floor(Date.now() / 1000); // Transformamos el valor del reloj a unix para hacer la comparacacion mas adelante..
			    	$(".evento-del-dia").each(function()
			    	{
			    		// Revisar si la fecha y hora actual se encuentra dentro del horario de algun evento..
			    		if ( relojUnix >= parseInt(this.dataset.unixInicio) && relojUnix <= parseInt(this.dataset.unixTermino) )
			    		{
			    			// Si ya tiene la etiqueta 'esta-ocurriendo-ahorita', salir de aqui.
			    			if ($(this).find("[data-happening-now]").length > 0)
				    		{
				    			return;
				    		};

				    		// Agregar la etiqueta al panel del evento, para indicar que 'Esta sucediendo en estos momentos'.
			    			$(this).find("a.evento-dia-titulo").append("<span class='alert label right' data-happening-now>El evento está sucediendo en este momento.</span>");
			    		}
			    		else
			    		{
			    			// Si no tiene etiqueta de 'esta-ocurreidno-ahorita', salir de aqui.
			    			if ($(this).find("[data-happening-now]").length === 0)
				    		{
				    			return;
				    		};

				    		// Quita la etiqueta 'esta-ocurriendo-ahorita', el evento ya termino.
			    			$(this).find("[data-happening-now]").remove();
			    		};
			    	});
				},
				interval : window.setInterval(function()
			    {
			    	reloj.run();
			    }, 1000)
			};
			var eventos =
			{
				delDia : document.getElementById("eventos-del-dia"),
				delMes : document.getElementById("eventos-del-mes"),
				obtenerDelDia : function (dia, mes, temporada, verTab)
				{
					//$(modal.cargando).foundation("reveal", "open");
					$("#eventos-dia-fecha").html(lpad(dia,2,"0")+"/"+lpad(mes,2,"0")+"/"+temporada);
					// Cargar los eventos del día.
					$.post( "../php/api.php",
					{
						accion: "obtener-eventos-dia",
						dia : dia,
						mes : mes,
						temporada : temporada
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
					  		var resultado = data.resultado;
					  		$("#eventos-dia").html("");

					  		if (resultado.length === 0)
					  		{
					  			$("#eventos-dia").addClass("hide");
					  			$("#no-hay-eventos-dia").removeClass("hide");
					  		}
					  		else
					  		{
					  			$("#eventos-dia").removeClass("hide");
					  			$("#no-hay-eventos-dia").addClass("hide");

					  			for (var i = 0; i < resultado.length; i++)
							  	{
							  		var evento = resultado[i];
							  		var convocados = evento.convocados;
							  		var active = "";
							  		var asistencia = evento.asistencia === "1" ? "checked='true'" : "";

							  		var lista_convocados = document.createElement("ul");
						  			lista_convocados.style.listStyleType = "square";
						  			lista_convocados.style.fontSize = "12px";

							  		for (var j = 0; j < convocados.length; j++)
							  		{
							  			lista_convocados.innerHTML += "<li>"+convocados[j].nombre+"</li>";
							  		}

							  		$("#eventos-dia").append("<li class='accordion-navigation evento-del-dia' data-unix-inicio='"+
						  					evento.unix_inicio+"' data-unix-termino='"+evento.unix_termino+"' data-id='"+evento.id_evento+"'>"+
										"<a class=evento-dia-titulo href='#panel"+i+"' style='padding:0;'>"+
											"<table style='width:100%;margin-bottom:0;'>"+
												"<tr>"+
													"<td><strong style=font-size:12px>"+evento.hora_inicio+" - "+evento.hora_termino+"</strong>: <span style='font-size:14px;'>"+evento.titulo+"</span></td>"+
													"<td><div class='switch round right'><div><img src='../css/img/cargando.gif' class='switch-loading hide'></div><input id='on-off-"+i+"' class='switch-checkbox asistencia-evento' type='checkbox' "+asistencia+"><label for='on-off-"+i+"' class='switch-on-off'><span class='switch-on'>Sí</span><span class='switch-off'>No</span></label><small class='switch-label'>Asistiré</small></div></td>"+
												"</tr>"+
											"</table>"+
										"</a>"+
										"<div id='panel"+i+"' class='content "+active+"' style='position:relative;'>"+
											//"<small class=tiny-note-left><em>Han confirmado asistencia <strong>"+evento.confirmados+"</strong> de <strong>"+evento.convocados+"</strong></em>.</small>"+
											"<small class=tiny-note-right><em>Creado por <strong>"+evento.creador+"</strong></em>.</small>"+
											"<p style='margin:0'>&#151;&nbsp;"+evento.descripcion+"</p>"+
											"<p style='margin:5px 0 5px 0;'>Hora de llegada del Alcalde <b>"+evento.hora_llegada+"</b> y se retira a las <b>"+evento.hora_salida+"</b>.</p>"+
											"<table class='panel-extra-info'>"+
												"<caption>INFORMACION ADICIONAL</caption>"+
												"<tr><td><span style='font-size:12px'><strong>Representante:</strong></td><td class='td-representante'>"+evento.representante+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Responsable:</strong></td><td>"+evento.responsable+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Vestimenta:</strong></td><td>"+evento.vestimenta+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Logística:</strong></td><td>"+evento.logistica+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Convocados:</strong></td><td>"+lista_convocados.outerHTML+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Ubicación:</strong></td><td>"+evento.ubicacion_dir+"</span></td></tr>"+
												"<tr><td><span style='font-size:12px'><strong>Publicado:</strong></td><td>"+evento.publicado+"</span></td></tr>"+
											"</table>"+
											//"<a target='_blank' title='Da clic para ver la ubicación del evento.' href='http://maps.google.com?q="+evento.ubicacion_lat+","+evento.ubicacion_long+"'>"+
											//"Da clic aquí para verlo en Google Maps.</a>"+
											"<a href='imprimir-mapa.php?&q="+evento.ubicacion_lat+","+evento.ubicacion_lng+"' style='color:#CB2B2B;text-decoration:underline;font-size:14px;' target='_blank'>Para imprimir el mapa da clic <b>aqui</b>.</a>"+
											"<iframe width='100%' height='250' frameborder='0' style='border:0'"+
											"src='https://www.google.com/maps/embed/v1/place?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4"+
											"&q="+evento.ubicacion_lat+","+evento.ubicacion_lng+"' allowfullscreen>"+
											"</iframe>"+
											//"<img style='width:100%;' src='https://maps.googleapis.com/maps/api/staticmap?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4"+
											//"&markers="+evento.ubicacion_lat+","+evento.ubicacion_lng+"&zoom=14&size=320x100'>"+
											"<div class=row>"+
												<?php
													echo '"';
													if (in_array("0001",$claves) || in_array("3001",$claves))
													{
														echo "<div class='large-4 medium-4 small-12 columns end'><a href='#delete' class='eliminar-evento tiny alert button expand'>Eliminar</a></div>";
													}

													if (in_array("0001",$claves) || in_array("2002",$claves))
													{
														echo "<div class='large-4 medium-4 small-12 columns end'><a href='#sendRep' class='mandar-representante-evento tiny button expand'>Mandar Representante</a></div>";
													}

													if (in_array("0001",$claves) || in_array("2001",$claves))
													{
														echo "<div class='large-4 medium-4 small-12 columns end'><a href='#edit' class='editar-evento tiny button expand'>Editar</a></div>";
													}

													echo '"+';
												?>"</div>"+
										"</div>"+
									"</li>");
							  	};
					  		};

					  		if (verTab)
					  		{
					  			$("[href='#panel-eventos-dia']").click();
					  		};
					  	};

					  	//$(modal.cargando).foundation("reveal", "close");
					}, "json");
				}
			}
			var calendario =
			{
				atras : document.getElementById("calendar-back"),
				mes : document.getElementById("calendar-month"),
				temporada : document.getElementById("calendar-year"),
				adelante : document.getElementById("calendar-forward")
			};
			var modal =
			{
				evento : document.getElementById("eventos-dia-modal"),
				cargando : document.getElementById("cargando-modal")
			};

			// Mask Init.
			$(".fecha").mask("00/00/0000", { clearIfNotMatch: true });
			$(".tiempo").mask("00:00", { clearIfNotMatch: true });

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			calendario.mes.onchange = function()
			{
				getFoundationCalendar(this.value, calendario.temporada.value);
			};

			calendario.temporada.onchange = function()
			{
				getFoundationCalendar(calendario.mes.value, this.value);
			};

			calendario.atras.onclick = function()
			{
				if (calendario.mes.selectedIndex === 0)
				{
					if (calendario.temporada.selectedIndex === 0)
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex -= 1;
					}

					calendario.mes.options[calendario.mes.options.length-1].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex-1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			calendario.adelante.onclick = function()
			{
				if (calendario.mes.selectedIndex === (calendario.mes.options.length-1))
				{
					if (calendario.temporada.selectedIndex === (calendario.temporada.options.length-1))
					{
						return;
					}
					else
					{
						calendario.temporada.options.selectedIndex += 1;
					}

					calendario.mes.options[0].selected = true;
				}
				else
				{
					calendario.mes.options[calendario.mes.selectedIndex+1].selected = true;
				}

				getFoundationCalendar(calendario.mes.value, calendario.temporada.value);
			};

			// Evento para cargar el evento del día seleccionado en el calendario.
			$(eventos.delMes).on("click", "li.week div.day", function()
			{
				eventos.obtenerDelDia(this.dataset.dia, this.dataset.mes, this.dataset.temporada, true);
			});

			// Evento para recargar los eventos del día de hoy.
			$("#agendar-evento").click(function()
			{
				if ($("#guardar-cambios-evento").attr("data-id") !== "")
				{
					alert("¡Espera! En estos momentos estas EDITANDO la información de este evento.");
					$("[href='#panel-agendar-eventos'").click(); // Lo mueve a la tab de Edición.
					$("#guardar-cambios-evento").focus();
					alert("Por favor, GUARDA o CANCELA los cambios antes de agendar un evento.");

					return event.preventDefault();
				}

				var rightNow = new Date();
				var horaInicio = lpad(rightNow.getHours() + 1, 2, "0");
				var horaTermino  = lpad(rightNow.getHours() + 2, 2, "0");

				$("#fecha-inicio-evento").val($("#eventos-dia-fecha").text());
				$("#hora-inicio-evento").val(horaInicio+":00");
				$("#fecha-termino-evento").val($("#eventos-dia-fecha").text());
				$("#hora-termino-evento").val(horaTermino+":00");
				$("[href='#panel-agendar-eventos']").click();
				$("#titulo-evento").focus();
			});

			// Evento para recargar los eventos del día de hoy.
			$("#descargar-eventos").click(function(event)
			{
				var eventos = [];
				$(".evento-del-dia").each(function()
				{
					eventos.push(this.dataset.id);
				});

				if (eventos.length === 0)
				{
					alert("No hay eventos para este día.");
					return event.preventDefault();
				};

				descargarEventos(JSON.stringify(eventos));
			});

			// Evento para el switch de asistencia.
			$(document).on("click", ".switch-on-off", function()
			{
				// this.classList.add("small-opacity")

				var divSwitch = this.parentNode;
				// divSwitch.querySelector(".switch-checkbox").disabled = true;
				// divSwitch.querySelector(".switch-loading").classList.remove("hide");
				// divSwitch.querySelector(".switch-label").textContent = "Cargando...";

				var asistenciaEvento = divSwitch.querySelector(".switch-checkbox");
				asistenciaEvento.checked = !asistenciaEvento.checked;

				var confirmarAsistencia;
				var accionAsistencia;

				if (asistenciaEvento.checked)
				{
					confirmarAsistencia = confirm("¿Estás seguro de querer CONFIRMAR tu asistencia a éste evento?");
					accionAsistencia = "CONFIRMAR";
				}
				else
				{
					confirmarAsistencia = confirm("¿Estás seguro de querer CANCELAR tu asistencia a éste evento?");
					accionAsistencia = "CANCELAR";
				};
				

				if (confirmarAsistencia)
				{
					$(modal.cargando).foundation("reveal", "open");
					var eventoDelDia = $(this).closest(".evento-del-dia");

					$.post("../php/api.php",
					{
						accion: "confirmar-asistencia",
						id: eventoDelDia.attr("data-id"),
						asistencia: asistenciaEvento.checked
					}, function (data)
					{
						if (data.status === "OK")
						{
							var resultado = data.resultado;

							if (resultado.es_alcalde && resultado.asistencia)
							{
								eventoDelDia.find(".td-representante").html("El alcalde SI asistirá.");
							}
							else if (resultado.es_alcalde && !resultado.asistencia)
							{
								if (resultado.representante === "")
								{
									eventoDelDia.find(".td-representante").html("El alcalde NO asistirá.");
								}
								else
								{
									eventoDelDia.find(".td-representante").html("El alcalde NO asistirá. En cambio, ira " + representante + " en su representación.");
								}
							}

							alert("!Éxito¡ Acabas de " + accionAsistencia + " tu asistencia.");

							// if (asistenciaEvento.checked)
							// {
							// 	eventoDelDia.find(".td-representante").html("El alcalde SI asistirá.");
							// }
							// else
							// {
							// 	var resultado = data.resultado,
							// 	representante = resultado.representante;

							// 	if (representante === "")
							// 	{
							// 		eventoDelDia.find(".td-representante").html("El alcalde NO asistirá.");
							// 	}
							// 	else
							// 	{
							// 		eventoDelDia.find(".td-representante").html("El alcalde NO asistirá. En cambio, ira " + representante + " en su representación.");
							// 	}
							// }
						}
						else
						{
							alert("¡Oh no! Algo sucedió.\nPor favor, intenta confirmar o cancelar tu asistencia de nuevo.");
						};

						setTimeout(function() {
							$(modal.cargando).foundation("reveal", "close");
						}, 1);
					}, "json");
				}
				else
				{
					asistenciaEvento.checked = !asistenciaEvento.checked;
				};
			});

			// Evento para el boton crear-evento.
			$(document).on("click", "#crear-evento", function()
			{
				var requiredEmpty = false;
				var emptyRequired = "";
				$("#form-evento [required]").each(function()
				{
					if (this.value === "") {
						if (!requiredEmpty) { this.focus(); };
						requiredEmpty = true;
						emptyRequired += "\t- " + this.dataset.title + "\n";
						// return false;
					}
				});

				if ( requiredEmpty ) { alert("Por favor llena todos los campos marcados con *\n\n"+emptyRequired); return false; };

				var confirmarCrearEvento = confirm("¿Estás seguro de querer CREAR este evento?");
				var codigo = "";

				if (confirmarCrearEvento)
				{
					if ($("#publicado-evento").prop("checked"))
					{
						codigo = prompt("Favor de ingresar el código de acceso");
					}

					$(modal.cargando).foundation("reveal", "open");

					$.post("../php/api.php",
					{
						accion: "crear-evento",
						titulo: $("#titulo-evento").val(),
						descripcion: $("#descripcion-evento").val(),
						responsable: $("#responsable-evento").val(),
						vestimenta: $("#vestimenta-evento").val(),
						logistica: $("#logistica-evento").prop("checked"),
						convocados_alcalde: $("#convocados-alcalde-evento").prop("checked"),
						convocados_cabildo: $("#convocados-cabildo-evento").prop("checked"),
						convocados_secretarios: $("#convocados-secretarios-evento").prop("checked"),
						convocados_directores: $("#convocados-directores-evento").prop("checked"),
						convocados_jefes: $("#convocados-jefes-evento").prop("checked"),
						convocados_dif: $("#convocados-dif-evento").prop("checked"),
						publicado: $("#publicado-evento").prop("checked"),
						fecha_inicio: $("#fecha-inicio-evento").val(),
						hora_inicio: $("#hora-inicio-evento").val(),
						fecha_termino: $("#fecha-termino-evento").val(),
						hora_termino: $("#hora-termino-evento").val(),
						hora_llegada: $("#hora-llegada-alcalde").val(),
						hora_salida: $("#hora-salida-alcalde").val(),
						ubicacion_dir: $("#ubicacion-evento").val(),
						ubicacion_lat: $("#ubicacion-evento").attr("data-lat"),
						ubicacion_lng: $("#ubicacion-evento").attr("data-lng"),
						codigo: codigo
					}, function (data)
					{
						if (data.status === "OK")
						{
							var resultado = data.resultado;

							if ( $("#eventos-dia-fecha").text() === $("#fecha-inicio-evento").val() )
							{
								eventos.obtenerDelDia(resultado.dia_inicio, resultado.mes_inicio, resultado.temporada_inicio, true);
							}
							else if ( $("#eventos-dia-fecha").text() === $("#fecha-termino-evento").val() )
							{
								eventos.obtenerDelDia(resultado.dia_termino, resultado.mes_termino, resultado.temporada_termino, true);
							}
							else
							{
								var eventosDiaFecha = $("#eventos-dia-fecha").text(),
									eventoFechaSplit = eventosDiaFecha.split("/"),
									diaFecha = eventoFechaSplit[0],
									mesFecha = eventoFechaSplit[1],
									temFecha = eventoFechaSplit[2];

								eventos.obtenerDelDia(diaFecha, mesFecha, temFecha, false);
							}

							if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_inicio) && $("#calendar-year").val() === resultado.temporada_inicio )
							{
								getFoundationCalendar(resultado.mes_inicio, resultado.temporada_inicio);
							}
							else if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_termino) && $("#calendar-year").val() === resultado.temporada_termino )
							{
								getFoundationCalendar(resultado.mes_termino, resultado.temporada_termino);
							}

							alert("¡Evento creado con éxito!\n\n");

							limpiarFormEvento();
						}
						else if (data.status === "CODE ERROR")
						{
							alert("Código incorrecto.\n\nNo fue posible crear ni publicar el evento.");
						}
						else if (data.status === "PASSED DATE")
						{
							alert("Lo sentimos, no fue posible crear el evento.\n\nLa fecha u hora de inicio o de termino han expirado.");
							$("#fecha-inicio-evento").focus();
						}
						else if (data.status === "NOT TODAY")
						{
							alert("Lo sentimos, no es posible crear un evento para el día de hoy.");
						}
						else if (data.status === "NOT TOMORROW")
						{
							alert("Lo sentimos, no fue posible crear el evento.\n\nLa hora límite para crear o editar eventos para mañana son a las 14:00 hrs del día de hoy.");
						}
						else if (data.status === "DATE RANGE ERROR")
						{
							alert("¡Oops! Encontramos un error en las fechas.\n\nLa fecha / hora de termino es MENOR a la fecha / hora de inicio.");
							$("#hora-termino-evento").focus();
						}
						else if (data.status === "DUPLICATED")
						{
							alert("Lo sentimos.\nYa existe un evento durante el horario que has definido.\n\nTe sugerimos revisar el calendario para encontrar un horario disponible.");
						}
						else
						{
							alert("¡Oh no! Algo sucedió...\n\nDa clic en el botón 'Crear Evento' de nuevo porfavor.");
						}

						$(modal.cargando).foundation("reveal", "close");
					}, "json");
				};
			});
			
			// Evento para obtener la info de un evento con el fin de editarlo.
			$(document).on("click", ".editar-evento", function()
			{
				$(modal.cargando).foundation("reveal", "open");
				var id = $(this).closest(".evento-del-dia").attr("data-id");

				$.post("../php/api.php",
				{
					accion: "obtener-evento",
					id: id
				}, function (data)
				{
					var evento = data.resultado;

					$("#titulo-evento").val(evento.titulo);
					$("#descripcion-evento").val(evento.descripcion);
					$("#responsable-evento").val(evento.responsable);
					$("#vestimenta-evento").val(evento.vestimenta);
					$("#logistica-evento").prop("checked", parseInt(evento.logistica));
					$("#convocados-alcalde-evento").prop("checked", parseInt(evento.convocados.alcalde));
					$("#convocados-cabildo-evento").prop("checked", parseInt(evento.convocados.cabildo));
					$("#convocados-secretarios-evento").prop("checked", parseInt(evento.convocados.secretarios));
					$("#convocados-directores-evento").prop("checked", parseInt(evento.convocados.directores));
					$("#convocados-jefes-evento").prop("checked", parseInt(evento.convocados.jefes));
					$("#convocados-dif-evento").prop("checked", parseInt(evento.convocados.dif));
					$("#publicado-evento").prop("checked", parseInt(evento.publicado));
					$("#fecha-inicio-evento").val(evento.fecha_inicio);
					$("#hora-inicio-evento").val(evento.hora_inicio);
					$("#fecha-termino-evento").val(evento.fecha_termino);
					$("#hora-termino-evento").val(evento.hora_termino);
					$("#hora-llegada-alcalde").val(evento.hora_llegada);
					$("#hora-salida-alcalde").val(evento.hora_salida);
					$("#ubicacion-evento").val(evento.ubicacion_dir);
					$("#ubicacion-evento").attr("data-lat", evento.ubicacion_lat);
					$("#ubicacion-evento").attr("data-lng", evento.ubicacion_lng);

					if (marker !== undefined)
					{
						marker.setMap(null);
					};

					marker = new google.maps.Marker(
					{
				    	position: new google.maps.LatLng(evento.ubicacion_lat, evento.ubicacion_lng),
				    	map: map
				  	});

				  	map.panTo(marker.getPosition());
				  	map.setZoom(17);

				  	$("#contenedor-crear-evento").addClass("hide");
				  	$("#contenedor-guardar-cambios-evento").removeClass("hide");
				  	$("#guardar-cambios-evento").attr("data-id", id);

				  	$("#titulo-evento").focus();

				  	setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
				}, "json");

				$("[href='#panel-agendar-eventos'").click();
			});

			$(document).on("click", ".mandar-representante-evento", function(event)
			{
				var promptRepresentante = prompt("Escribe el nombre del representante:");
				var eventoDelDia = $(this).closest(".evento-del-dia");

				if (promptRepresentante !== null /*&& promptRepresentante !== ""*/)
				{
					$(modal.cargando).foundation("reveal", "open");

					$.post("../php/api.php",
					{
						accion: "mandar-representante-evento",
						id: eventoDelDia.attr("data-id"),
						representante: promptRepresentante
					}, function (data)
					{
						if (data.status === "OK")
						{
							alert("El representante fue ingresado con éxito.");

							if (eventoDelDia.find("input.asistencia-evento").prop("checked"))
							{
								eventoDelDia.find(".td-representante").html("El alcalde SI asistirá.");
							}
							else
							{
								if (promptRepresentante !== "")
								{
									eventoDelDia.find(".td-representante").html("El alcalde NO asistirá. En cambio, ira "+promptRepresentante.toUpperCase()+" en su representación.");
								}
								else
								{
									eventoDelDia.find(".td-representante").html("El alcalde NO asistirá.");
								}
							}
						}
						else
						{
							alert("¡Oh no! Algo sucedió, el representante NO fue capturado.\n\nPor favor inténtalo de nuevo.");
						}

						setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
					}, "json");
				}
			});

			$(document).on("click", ".eliminar-evento", function(event)
			{
				var confirmarEliminacion = confirm("¿Estás seguro que deseas ELIMINAR éste evento?");

				if (confirmarEliminacion)
				{
					var eventoDelDia = $(this).closest(".evento-del-dia");

					// Revisar si se encuentra editando este evento que quiere eliminar.
					if ($("#guardar-cambios-evento").attr("data-id") === eventoDelDia.attr("data-id"))
					{
						alert("¡Espera! En estos momentos estas EDITANDO la información de este evento.");
						$("[href='#panel-agendar-eventos'").click(); // Lo mueve a la tab de Edición.
						$("#guardar-cambios-evento").focus();
						alert("Por favor, GUARDA o CANCELA los cambios antes de eliminar el evento.");

						return event.preventDefault();
					}

					$(modal.cargando).foundation("reveal", "open");

					$.post("../php/api.php",
					{
						accion: "eliminar-evento",
						id: eventoDelDia.attr("data-id")
					}, function (data)
					{
						if (data.status === "OK")
						{
							var resultado = data.resultado;

							if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_inicio) && $("#calendar-year").val() === resultado.temporada_inicio )
							{
								getFoundationCalendar(resultado.mes_inicio, resultado.temporada_inicio);
							}
							else if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_termino) && $("#calendar-year").val() === resultado.temporada_termino )
							{
								getFoundationCalendar(resultado.mes_termino, resultado.temporada_termino);
							};

							alert("El evento fue eliminado con éxito.");
							eventoDelDia.remove();
						}
						else if (data.status === "PASSED DATE")
						{
							alert("No es posible eliminar este evento. Es público y esta sucediendo en estos momentos o ya pasó.");
						}
						else
						{
							alert("¡Oops! Algo sucedió, el evento NO fue eliminado.\n\nPor favor inténtalo de nuevo.");
						}

					  	setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
					}, "json");
				}
			});

			// Evento para guardar los cambios de un evento.
			$("#guardar-cambios-evento").click(function()
			{
				var confirmarGuardarCambios = confirm("¿Estás seguro de querer GUARDAR los cambios?");
				var codigo = "";

				if (confirmarGuardarCambios)
				{
					if ($("#publicado-evento").prop("checked"))
					{
						codigo = prompt("Favor de ingresar el código de acceso");
					}

					$(modal.cargando).foundation("reveal", "open");

					$.post("../php/api.php",
					{
						accion: "guardar-cambios-evento",
						id: $("#guardar-cambios-evento").attr("data-id"),
						titulo: $("#titulo-evento").val(),
						descripcion: $("#descripcion-evento").val(),
						responsable: $("#responsable-evento").val(),
						vestimenta: $("#vestimenta-evento").val(),
						logistica: $("#logistica-evento").prop("checked"),
						convocados_alcalde: $("#convocados-alcalde-evento").prop("checked"),
						convocados_cabildo: $("#convocados-cabildo-evento").prop("checked"),
						convocados_secretarios: $("#convocados-secretarios-evento").prop("checked"),
						convocados_directores: $("#convocados-directores-evento").prop("checked"),
						convocados_jefes: $("#convocados-jefes-evento").prop("checked"),
						convocados_dif: $("#convocados-dif-evento").prop("checked"),
						publicado: $("#publicado-evento").prop("checked"),
						fecha_inicio: $("#fecha-inicio-evento").val(),
						hora_inicio: $("#hora-inicio-evento").val(),
						fecha_termino: $("#fecha-termino-evento").val(),
						hora_termino: $("#hora-termino-evento").val(),
						hora_llegada: $("#hora-llegada-alcalde").val(),
						hora_salida: $("#hora-salida-alcalde").val(),
						ubicacion_dir: $("#ubicacion-evento").val(),
						ubicacion_lat: $("#ubicacion-evento").attr("data-lat"),
						ubicacion_lng: $("#ubicacion-evento").attr("data-lng"),
						codigo: codigo
					}, function (data)
					{
						if (data.status === "OK")
						{
							var resultado = data.resultado;

							if ( $("#eventos-dia-fecha").text() === $("#fecha-inicio-evento").val() )
							{
								eventos.obtenerDelDia(resultado.dia_inicio, resultado.mes_inicio, resultado.temporada_inicio, false);
							}
							else if ( $("#eventos-dia-fecha").text() === $("#fecha-termino-evento").val() )
							{
								eventos.obtenerDelDia(resultado.dia_termino, resultado.mes_termino, resultado.temporada_termino, false);
							}
							else
							{
								var eventosDiaFecha = $("#eventos-dia-fecha").text(),
									eventoFechaSplit = eventosDiaFecha.split("/"),
									diaFecha = eventoFechaSplit[0],
									mesFecha = eventoFechaSplit[1],
									temFecha = eventoFechaSplit[2];

								eventos.obtenerDelDia(diaFecha, mesFecha, temFecha, false);
							}

							if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_inicio) && $("#calendar-year").val() === resultado.temporada_inicio )
							{
								getFoundationCalendar(resultado.mes_inicio, resultado.temporada_inicio);
							}
							else if ( parseInt($("#calendar-month").val()) === parseInt(resultado.mes_termino) && $("#calendar-year").val() === resultado.temporada_termino )
							{
								getFoundationCalendar(resultado.mes_termino, resultado.temporada_termino);
							}

							alert("¡Cambios guardados con éxito!\n\n");

							limpiarFormEvento();
						}
						else if (data.status === "CODE ERROR")
						{
							alert("Código erroneo.\n\nNo fue posible editar ni publicar el evento.");
						}
						else if (data.status === "PASSED DATE")
						{
							alert("Lo sentimos, no fue posible guardar los cambios del evento.\n\nLa fecha u hora de inicio o de termino han expirado.");
							$("#fecha-inicio-evento").focus();
						}
						else if (data.status === "NOT TODAY")
						{
							alert("Lo sentimos, no es posible guardar los cambios de un evento del día de hoy.\n\n.");
						}
						else if (data.status === "NOT TOMORROW")
						{
							alert("Lo sentimos, no fue posible guardar los cambios del evento.\n\nLa hora límite para crear o editar eventos para mañana son a las 14:00 hrs del día de hoy.");
						}
						else if (data.status === "DATE RANGE ERROR")
						{
							alert("¡Oops! Encontramos un error en las fechas.\n\nLa fecha / hora de termino es MENOR a la fecha / hora de inicio.");
							$("#hora-termino-evento").focus();
						}
						else if (data.status === "DUPLICATED")
						{
							alert("¡Lo sentimos!\nYa existe un evento durante el horario que has definido.\n\nTe sugerimos revisar el calendario para encontrar un horario disponible.");
						}
						else
						{
							alert("¡Oh no! Algo sucedió...\n\nDa clic en el botón 'Guardar cambios' de nuevo porfavor.");
						}

						$(modal.cargando).foundation("reveal", "close");
					}, "json");
				};
			});

			// Evento para cancelar la edición de un evento.
			$("#cancelar-cambios-evento").click(function()
			{
				var confirmarCancelacion = confirm("¿Estás seguro de querer CANCELAR los cambios?");
				if (confirmarCancelacion)
				{
					limpiarFormEvento();
				};
			});

			var limpiarFormEvento = function ()
			{
				$(":input","#form-evento", "textarea")
			    	.not(":button, :submit, :reset, :hidden")
			    	.val("")
			    	.removeAttr("checked")
			    	.removeAttr("selected");

			    if (marker !== undefined)
				{
					marker.setMap(null);
				};

				map.setZoom(12);
				map.setCenter({lat: 22.401298, lng: -97.925863});

			    $("#ubicacion-evento").attr("data-lat", "");
				$("#ubicacion-evento").attr("data-lng", "");

				$("#contenedor-guardar-cambios-evento").addClass("hide");
				$("#contenedor-crear-evento").removeClass("hide");
				$("#guardar-cambios-evento").attr("data-id", "");

				$("#titulo-evento").focus();
			};

			(function ()
			{
				$(modal.cargando).foundation("reveal", "open");
				// Correr el reloj.
				reloj.run();

				// Obtener Eventos del Día
				eventos.obtenerDelDia(dia, mes, temporada, true);

				//Llamar a la creación del Calendario
				calendario.mes.value = mes;
				calendario.temporada.value = temporada;
				getFoundationCalendar(mes, temporada);

				setTimeout(function() { $(modal.cargando).foundation("reveal", "close") }, 1);
			})();
		};
	</script>
	<?php
		if (in_array("0001",$claves) || in_array("1005",$claves))
		{
			echo "<script type=\"text/javascript\">
				var map;
				var geocoder;
				var marker;
				function initMap()
				{
					geocoder = new google.maps.Geocoder();
					map = new google.maps.Map(document.getElementById(\"map\"),
					{
						center: {lat: 22.401298, lng: -97.925863},
						zoom: 13
					});

					map.addListener('click', function( event )
					{
						if (marker !== undefined)
						{
							marker.setMap(null);
						};

						marker = new google.maps.Marker(
						{
					    	position: event.latLng,
					    	map: map,
					  	});

						// Geocoder to get the aprox. address
						geocoder.geocode( {'latLng': event.latLng},
							function(results, status) {
								if(status == google.maps.GeocoderStatus.OK) {
									if(results[0]) {
										document.getElementById('ubicacion-evento').value = results[0].formatted_address;
									}
									else {
										document.getElementById('ubicacion-evento').value = '';
									}
								}
							}
						);

						$('#ubicacion-evento').attr('data-lat', event.latLng.lat()).attr('data-lng', event.latLng.lng()).blur();
					});

					map.addListener('drag', function()
					{
						$('#ubicacion-evento').blur();
					});

					// Create the search box and link it to the UI element.
					var input = document.getElementById('ubicacion-evento');
					var searchBox = new google.maps.places.SearchBox(input);
					// map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

										  // Bias the SearchBox results towards current map's viewport.
					map.addListener('bounds_changed', function() {
						searchBox.setBounds(map.getBounds());
					});

				  	// [START region_getplaces]
				  	// Listen for the event fired when the user selects a prediction and retrieve
				  	// more details for that place.
					searchBox.addListener('places_changed', function() {
						var places = searchBox.getPlaces();
						var place = places[0];

						if (places.length == 0) {
							return;
						}

						// Clear out the old markers.
						if (marker !== undefined)
						{
							marker.setMap(null);
						};

						// For each place, get the icon, name and location.
						var bounds = new google.maps.LatLngBounds();
						// places.forEach(function(place) {
						// Create a marker for each place.
						marker = new google.maps.Marker(
						{
					    	position: place.geometry.location,
					    	map: map
					  	});
		
						// Set Latitude and Longitude.
						$('#ubicacion-evento').attr('data-lat', place.geometry.location.lat()).attr('data-lng', place.geometry.location.lng())

						if (place.geometry.viewport) {
							// Only geocodes have viewport.
							bounds.union(place.geometry.viewport);
						} else {
							bounds.extend(place.geometry.location);
						}
						// });
						map.fitBounds(bounds);
						map.setZoom(15);
					});
					// [END region_getplaces]
				}
			</script>
			<script
			  src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyBQDMw_UZ1zODQLYwpHQ_NXJnQo5xT_St4&libraries=places\">
			</script>";
		}
	?>
</body>
</html>