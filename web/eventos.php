<?php
	session_name("agenda_sia_2015");
	session_start();

	if ( !isset($_SESSION["usuario"]) )
	{
		header("Location: ../");
		exit;
	}

	$usuario_nombre = $_SESSION["usuario"]["nombre"];
	$permisos = $_SESSION["usuario"]["permisos"];
	
	$permiso_concedido = false;
	for ($i = 0; $i < count($permisos); $i++)
	{
		if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1003")
		{
			$permiso_concedido = true;
		}
	}

	if (!$permiso_concedido)
	{
		header("Location: ../index.php?e=2");
		exit;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Agenda SIA | Aplicación de Eventos Calendarizados</title>
    <link rel="shortcut icon" href="../favicon.ico">
    <link rel="stylesheet" href="../css/normalize.css">
    <link rel="stylesheet" href="../css/foundation.min.css" />
    <link rel="stylesheet" href="../css/foundation.calendar.css">
    <link rel="stylesheet" href="../css/jquery.datetimepicker.css">
    <script src="../js/vendor/modernizr.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=places"></script>
	<script>
		var placeSearch, autocomplete;
		var componentForm = {
		  	street_number: 'short_name',
		  	route: 'long_name',
		  	locality: 'long_name',
		  	administrative_area_level_1: 'short_name',
		  	country: 'long_name',
		  	postal_code: 'short_name'
		};

		function initialize_gmaps() 
		{
		  	// Create the autocomplete object, restricting the search
		  	// to geographical location types.
		  	autocomplete = new google.maps.places.Autocomplete(
		  		/** @type {HTMLInputElement} */(document.getElementById('ubicacion-dir')),
		  		{ types: ['geocode'], componentRestrictions: {country: 'mx'} });
		  	// When the user selects an address from the dropdown,
		  	// populate the address fields in the form.
		  	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		  		fillInAddress();
		  	});
		}

		function fillInAddress()
		{
		  	// Get the place details from the autocomplete object.
		  	var place = autocomplete.getPlace();
		  	
		  	if (place.geometry === undefined)
		  	{
		  		document.querySelector("input[name='ubicacion-lat']").value = 0;
		  		document.querySelector("input[name='ubicacion-lng']").value = 0;
		  		return;
		  	};

		  	document.querySelector("input[name='ubicacion-lat']").value = place.geometry.location.lat();
		  	document.querySelector("input[name='ubicacion-lng']").value = place.geometry.location.lng();
		}
		// [END region_fillform]

		// [START region_geolocation]
		// Bias the autocomplete object to the user's geographical location,
		// as supplied by the browser's 'navigator.geolocation' object.
		function geolocate()
		{
	  		if (navigator.geolocation) {
	    		navigator.geolocation.getCurrentPosition(function(position) {
	      			var geolocation = new google.maps.LatLng(
	          		position.coords.latitude, position.coords.longitude);
	      			var circle = new google.maps.Circle({
	        				center: geolocation,
	        				radius: position.coords.accuracy
	      				});
	      			autocomplete.setBounds(circle.getBounds());
	    		});
	  		}
		}
	</script>
</head>
<body>
	<nav id="top-bar-principal" class="top-bar" data-topbar>
		<ul class="title-area">
			<li class="name">
				<h1><a href="#">Agenda SIA</a></h1>
			</li>
			<!-- <small class="show-for-small-only"><?php //echo "Bienvenido $usuario_nombre"; ?></small>-->

			<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
		</ul>

		<section class="top-bar-section">
			<ul class="right">
				<?php
					for ($i = 0; $i < count($permisos); $i++)
					{
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1001") { echo "<li><a href='index.php'>Inicio</a></li>"; }
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1002") { echo "<li><a href='agendas.php'>Agendas</a></li>"; }
						if ($permisos[$i]["clave"] === "0001" || $permisos[$i]["clave"] === "1003") { echo "<li><a href='eventos.php'>Eventos</a></li>"; }
					}
				?>
				<!-- <li><a href="index.php">Inicio</a></li> -->
				<!-- <li class="has-dropdown">
					<a href="#">Evento</a>
					<ul class="dropdown">
						<li><a class="evento" href="#">Crear Evento</a></li>
						<li><a class="evento" href="#">Editar Evento</a></li>
					</ul>
				</li> -->
				<li><a id="cerrar-sesion" href="#">Cerrar Sesión</a></li>
			</ul>

			<ul class="left hide-for-small-only">
				<li><a href="#"><?php echo "Bienvenido <b>$usuario_nombre</b>"; ?></a></li>
			</ul>
		</section>
	</nav>

	<header>
		<div class="row">
			<div class="large-12 columns">
				<h1>Eventos</h1>
				<p class="subheader">Da clic en el botón <strong>Cargar</strong> para ver los detalles de un evento. Posteriormente, lo puedes <strong>Editar</strong>.</p>
			</div>
		</div>
	</header>

	<div class="row">
		<div class="large-12 columns">
			<form id="formulario-evento" action="../php/api.php" method="POST">
				<div class="row">
					<div class="large-12 columns">
						<label for="agenda">Agenda</label>
						<select name="agenda" id="agenda"></select>
					</div>

					<div class="large-8 medium-8 columns">
						<label for="evento">Evento</label>
						<select name="evento" id="evento"></select>
					</div>

					<div class="large-4 medium-4 columns">
						<p class="hide-for-small-only"></p>
						<input id="cargar-evento" class="button small expand" type="button" value="Cargar">
					</div>

					<div class="large-12 columns">
						<hr>
						<p class="subheader">Llena los siguientes campos para <strong>Crear un Nuevo Evento</strong>.
							Se creará en la agenda <strong id="nombre-agenda"></strong>.
						</p>
					</div>

					<?php if(isset($_GET["e"]))
						{
							if ($_GET["e"] === "-2")
							{
								echo "<div class='large-12 columns'><div data-alert class='alert-box success'>¡El evento fue <strong>editado</strong> con éxito!<a href='#' class='close'>&times;</a></div>";
							}
							else if ($_GET["e"] === "-1")
							{
								echo "<div class='large-12 columns'><div data-alert class='alert-box success'>¡El evento fue <strong>creado</strong> con éxito!<a href='#' class='close'>&times;</a></div>";
							}
							else if ($_GET["e"] === "1")
							{
								echo "<div class='large-12 columns'><small class='error'>¡Oops! La <strong>fecha inicial</strong> fue mayor que la <strong>fecha de termino</strong>. Favor de intentarlo de nuevo.</small>";
							}
							else if ($_GET["e"] === "2")
							{
								echo "<div class='large-12 columns'><small class='error'>Ya existe un evento en la agenda a la misma hora. Favor de revisar que los eventos no se empalmen.</small>";
							}
							else if ($_GET["e"] === "3")
							{
								echo "<div class='large-12 columns'><small class='error'>Algo sucedió. Favor de volver a intentar crear el evento.</small>";
							}
						}
					?>

					<div class="large-12 columns">
						<label for="titulo">Título*</label>
						<input id="titulo" name="titulo" type="text" placeholder="Ingresa el título del evento." required>
					</div>

					<div class="large-12 columns">
						<label for="descripcion">Descripción</label>
						<textarea id="descripcion" name="descripcion" rows="5" placeholder="Ingresa la descripción detallada del evento."></textarea>
					</div>

					<div class="large-6 medium-6 columns">
						<label for="fecha-inicia">Fecha Inicia*</label>
						<input id="fecha-inicia" name="fecha-inicia" type="text" class="datetimepicker" placeholder="Ingresa la fecha de inicio." autocomplete="off" required>
					</div>

					<div class="large-6 medium-6 columns">
						<label for="fecha-termina">Fecha Termina*</label>
						<input id="fecha-termina" name="fecha-termina" type="text" class="datetimepicker" placeholder="Ingresa la fecha de termino." autocomplete="off" required>
					</div>

					<div class="large-12 columns">
						<label for="ubicacion-dir">Ubicación*</label>
						<input id="ubicacion-dir" name="ubicacion-dir" type="text" onfocus="geolocate()"
							onchange="document.getElementById('ubicacion-lat').value=0;document.getElementById('ubicacion-lng').value=0;"
							autocomplete="off" placeholder="Ingresa la ubicación." required>
						<input id="ubicacion-lat" name="ubicacion-lat" type="hidden">
						<input id="ubicacion-lng" name="ubicacion-lng" type="hidden">
					</div>

					<div class="large-12 columns">
						<input type="checkbox" id="cancelar-evento" name="cancelar-evento" style="width:24px;height:24px;vertical-align: top;">
						<label for="cancelar-evento">CANCELAR el evento.</label>
					</div>

					<div class="large-12 columns">
						<input type="checkbox" id="borrar-evento" name="borrar-evento" style="width:24px;height:24px;vertical-align: top;">
						<label for="borrar-evento">BORRAR el evento.</label>
					</div>

					<!-- <div class="large-12 columns">
						<label for="asistentes">Asistentes</label>
						<input id="asistentes" name="asistentes" type="text" required>
					</div> -->

					<!-- <div class="large-6 medium-6 columns">
						<label for="editar-evento">Editar</label>
						<input id="editar-evento" name="editar-evento" type="button" required>
					</div> -->

					<div class="large-6 medium-6 columns">
						<input id="editar-evento" name="editar-evento" type="submit" class="button small expand" value="Editar" disabled>
					</div>

					<div class="large-6 medium-6 columns">
						<input id="crear-nuevo-evento" name="crear-nuevo-evento" type="submit" class="button small expand" value="Crear Nuevo Evento">
					</div>

					<input type="hidden" name="accion" value="accion-evento">
				</div>
			</form>
		</div>
	</div>

	<div id="cargando-modal" class="tiny reveal-modal" data-reveal aria-hidden="true" role="dialog">
		<p class="text-center">Cargando... <img src="../css/img/cargando.gif"></p>
	</div>

	<script src="../js/vendor/jquery.js"></script>
    <script src="../js/vendor/jquery.datetimepicker.js"></script>
	<script src="../js/foundation.min.js"></script>
  	<script src="../js/foundation/foundation.topbar.js"></script>
  	<script src="../js/foundation/foundation.reveal.js"></script>
  	<script>$(document).foundation({
  		topbar :
  		{
			custom_back_text: false,
			is_hover: false,
			mobile_show_parent_link: false
		},
		reveal :
		{
			animation_speed: 0,
			close_on_background_click: false
		}
  	});</script>
	<script>
		// DateTimePicker initialize
		function initialize_datetimepicker(fechaInicia, fechaTermina)
		{
			jQuery("#fecha-inicia").datetimepicker(
			{
				lang : "es",
				theme : "default",
				inline : true,
				value : fechaInicia,
				format : "Y-m-d H:i",
				//minDate : 0,
				step : 15,
				onChangeDateTime : function(ct, $input)
				{
					
				}
			});

			jQuery("#fecha-termina").datetimepicker(
			{
				lang : "es",
				theme : "default",
				inline : true,
				value : fechaTermina,
				format : "Y-m-d H:i",
				//minDate : 0,
				step : 15,
				onChangeDateTime : function(ct, $input)
				{
					
				}
			});
		};
	</script>
	<script>
		window.onload = function()
		{
			// Inicializar la libreria de Google Maps
			initialize_gmaps();

			// Inicializar los datetimepicker
			initialize_datetimepicker(new Date(), new Date());

			// Variables
			var topBar =
			{
				cerrarSesion : document.getElementById("cerrar-sesion")
			};

			var agenda =
			{
				nombre : document.getElementById("nombre-agenda"),
				select : document.getElementById("agenda"),
				array : []
			};

			var evento =
			{
				select : document.getElementById("evento"),
				array  : [],
				form   : document.getElementById("formulario-evento"),
				cargar : document.getElementById("cargar-evento"),
				editar : document.getElementById("editar-evento"),
				crear  : document.getElementById("crear-nuevo-evento"),
				inputAccion : document.querySelector("input[name='accion']")
			};

			topBar.cerrarSesion.onclick = function()
			{
				$.post( "../php/api.php",
				{
					accion: "cerrar-sesion",
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		window.location.href = "../";
				  	}
				}, "json");
			};

			evento.cargar.onclick = function()
			{
				$("#cargando-modal").foundation("reveal", "open");

				$.post( "../php/api.php",
				{
					accion : "cargar-evento",
					id : evento.select.value
				}, function( data )
				{
				  	if ( data.status === "OK" )
				  	{
				  		var _evento = data.resultado;

				  		evento.form.querySelector("#titulo").value = _evento.titulo;
				  		evento.form.querySelector("#descripcion").value = _evento.descripcion;
				  		initialize_datetimepicker(_evento.fecha_inicia, _evento.fecha_termina);
				  		// evento.form.querySelector("#fecha-inicia").value = _evento.fecha_inicia;
				  		// evento.form.querySelector("#fecha-termina").value = _evento.fecha_termina;
				  		evento.form.querySelector("#ubicacion-dir").value = _evento.ubicacion_dir;
				  		evento.form.querySelector("#ubicacion-lat").value = _evento.ubicacion_lat;
				  		evento.form.querySelector("#ubicacion-lng").value = _evento.ubicacion_lng;
				  		evento.form.querySelector("#cancelar-evento").checked = _evento.cancelado === "0" ? false : true;

				  		$("#cargando-modal").foundation("reveal", "close");
				  		evento.editar.removeAttribute("disabled");
				  		evento.form.querySelector("#titulo").focus();
				  	}
				}, "json");
			};

			// Cargar Agendas.
			$("#cargando-modal").foundation("reveal", "open");

			$.post( "../php/api.php",
			{
				accion : "obtener-agendas"
			}, function( data )
			{
			  	if ( data.status === "OK" )
			  	{
			  		var _agendas = data.resultado;

			  		for (var i = 0; i < _agendas.length; i++)
			  		{
			  			$(agenda.select).append("<option value='"+_agendas[i]["id"]+"'>"+_agendas[i]["nombre"]+"</option>");
			  		};

			  		// Mostrar el nombre en el <p> de la agenda seleccionada.
			  		agenda.nombre.textContent = $(agenda.select).find("option:first").text();

			  		// Cargar Eventos.
			  		$.post( "../php/api.php",
					{
						accion : "obtener-eventos"
					}, function( data )
					{
					  	if ( data.status === "OK" )
					  	{
					  		var _eventos = data.resultado;
					  		evento.array = _eventos;

					  		for (var i = 0; i < _eventos.length; i++)
					  		{
					  			if ( $(agenda.select).val() === _eventos[i]["agenda"] )
								{
					  				$(evento.select).append("<option value='"+_eventos[i]["id"]+"' data-agenda='"+_eventos[i]["agenda"]+"'>"+_eventos[i]["titulo"]+"</option>");
					  			}
					  		};

					  		setTimeout(function() { $("#cargando-modal").foundation("reveal", "close"); }, 1);
					  	}
					}, "json");
			  	}
			}, "json");

			agenda.select.onchange = function()
			{
				agenda.nombre.textContent = this.options[this.selectedIndex].textContent;
				evento.select.options.length = 0;

				for (var i = 0; i < evento.array.length; i++)
				{
					if ( this.value === evento.array[i]["agenda"] )
					{
						$(evento.select).append("<option value='"+evento.array[i]["id"]+"' data-agenda='"+evento.array[i]["agenda"]+"'>"+evento.array[i]["titulo"]+"</option>");
					}
				}
			};

			evento.form.querySelector("input[type='text']").focus();
		};
	</script>
</body>
</html>