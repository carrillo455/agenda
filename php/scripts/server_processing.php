<?php

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
if ( !isset($_GET["option"]) ) {
	exit;
}

$option = (int) $_GET["option"];

switch ($option) {
	case 1:
		$table = '(select `hv`.`id` AS `id`,concat(`u`.`nombre`," ",`u`.`paterno`," ",`u`.`materno`) AS `nombre`,date_format(`hv`.`fecha_agenda`,"%d/%m/%Y") AS `agenda`,date_format(`hv`.`fecha_captura`,"%d/%m/%Y %H:%i:%s") AS `timestamp` from (`historial_vistas` `hv` join `usuarios` `u` on((`u`.`id_usuario` = `hv`.`id_usuario`))) order by `hv`.`fecha_captura` desc) a';
		// Table's primary key
		$primaryKey = 'id';

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'id', 'dt' => 0 ),
			array( 'db' => 'nombre', 'dt' => 1 ),
			array( 'db' => 'agenda', 'dt' => 2 ),
			array( 'db' => 'timestamp',  'dt' => 3 )
		);
		break;

	case 2:
		// $table = 'dt_asistencia';
		$table = '(select @i:=@i+1 AS consecutivo,e.titulo AS evento,concat(u.nombre," ",u.paterno," ",u.materno) AS nombre,if((a.asistencia = 1),"&#10003;","&#10007;") AS asistencia,date_format(a.fecha_captura,"%d/%m/%Y %H:%i:%s") AS timestamp from ((asistencias a join eventos e on((e.id_evento = a.id_evento))) JOIN (SELECT @i:=0) r join usuarios u on((u.id_usuario = a.id_usuario)))) a';
		// Table's primary key
		$primaryKey = 'consecutivo';

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'consecutivo', 'dt' => 0 ),
			array( 'db' => 'evento', 'dt' => 1 ),
			array( 'db' => 'nombre', 'dt' => 2 ),
			array( 'db' => 'asistencia',  'dt' => 3 ),
			array( 'db' => 'timestamp',   'dt' => 4 )
		);
		break;

	case 3:
		$table = '(select @i:=@i+1 AS consecutivo,e.titulo AS evento,concat(u.nombre," ",u.paterno," ",u.materno) AS usuario, b.accion, date_format(b.fecha_captura,"%d/%m/%Y %H:%i:%s") AS timestamp from ((bitacora b left join eventos e on((e.id_evento = b.id_evento))) JOIN (SELECT @i:=0) r join usuarios u on((u.id_usuario = b.id_usuario)))) a';
		// Table's primary key
		$primaryKey = 'consecutivo';

		// Array of database columns which should be read and sent back to DataTables.
		// The `db` parameter represents the column name in the database, while the `dt`
		// parameter represents the DataTables column identifier. In this case simple
		// indexes
		$columns = array(
			array( 'db' => 'consecutivo', 'dt' => 0 ),
			array( 'db' => 'usuario', 'dt' => 1 ),
			array( 'db' => 'evento', 'dt' => 2 ),
			array( 'db' => 'accion',  'dt' => 3 ),
			array( 'db' => 'timestamp',   'dt' => 4 )
		);
		break;
}



// SQL server connection information
$sql_details = array(
	'user' => 'root',
	'pass' => '',
	'db'   => 'agenda',
	'host' => 'localhost'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );

$my_where = "";

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $my_where )
, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
