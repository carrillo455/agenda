<?php
    require_once('../fpdf.php');

    class PDF extends FPDF
    {
        // Cabecera de página
        function Header()
        {
            $this->SetAutoPageBreak(true, 0.5);
            $this->Image('../img/cabecera.png',10,5);
            $this->SetFont('Times','',16);
            $this->Cell(0,0,utf8_decode("Agenda"),0,0,'C');
        }

        function Table($i,$data)
        {
            /***TABLA***/
            //CABECER
            $this->SetFont('Times','',8);
            $w  = $this->w;
            $x  = array(10);
            $y  = 48;
            $cw = array(($w/7), ($w/4), ($w/9), ($w/9), ($w/9), ($w/9));

            $this->SetXY($x[0],$y);
            $this->Cell($cw[0], 6,"CODIFICACION",1,0,'C');  //30

            array_push($x, ($x[0]+$cw[0]));
            $this->SetXY($x[1],$y);
            $this->Cell($cw[1], 6,"DESCRIPCION",1,0,'C'); //70

            array_push($x, ($x[1]+$cw[1]));
            $this->SetXY($x[2],$y);
            $this->Cell($cw[2], 6,"MARCA",1,0,'C');

            array_push($x, ($x[2]+$cw[2]));
            $this->SetXY($x[3],$y);
            $this->Cell($cw[3], 6,"MODELO",1,0,'C');

            array_push($x, ($x[3]+$cw[3]));
            $this->SetXY($x[4],$y);
            $this->Cell($cw[4], 6,"SERIE",1,0,'C');

            array_push($x, ($x[4]+$cw[4]));
            $this->SetXY($x[5],$y);
            $this->Cell($cw[5], 6,"ESTADO",1,0,'C');

            //CUERPO
            $this->SetXY(10,54);
            $this->SetFont('Times','',8);
            $this->SetWidths($cw);
            $columns = array();

            for ($i; $i < count($data); $i++) //FILAS
            {
                for ($j = 0; $j < count($data[$i]); $j++) //COLUMNAS
                {
                    array_push($columns, utf8_decode($data[$i][$j]));
                }

                if ($this->GetY() >= $this->h-60)
                {
                    $this->AddPage();
                    $this->Table($i,$data);
                    break;
                }

                $this->Row($columns,5,3);
                $columns = array();
            }
        }

        // Pie de página
        function Footer()
        {
            $this->SetY(-15);
            $this->SetFont('Times','',8);
            $this->Cell(0,10,$this->PageNo(),0,0,'R');
        }

        //PDF_MC_Table by OLIVER
        var $widths;
        var $aligns;

        function SetWidths($w)
        {
            //Set the array of column widths
            $this->widths=$w;
        }

        function SetAligns($a)
        {
            //Set the array of column alignments
            $this->aligns=$a;
        }

        function Row($data, $myNb, $myRowHeight)
        {
            //Calculate the height of the row
            $nb=0;
            for($i=0;$i<count($data);$i++)
                $nb=max($nb, $this->NbLines($this->widths[$i], $data[$i]));
            $h= $myNb*$nb;
            //Issue a page break first if needed
            $this->CheckPageBreak($h);
            //Draw the cells of the row
            for($i=0;$i<count($data);$i++)
            {
                $w=$this->widths[$i];
                $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
                //Save the current position
                $x=$this->GetX();
                $y=$this->GetY();
                //Draw the border
                $this->Rect($x, $y, $w, $h);
                //Print the text
                $this->SetXY($x, $y+1);
                $this->MultiCell($w, $myRowHeight, $data[$i], 0, $a);
                //Put the position to the right of the cell
                $this->SetXY($x+$w, $y);
            }
            //Go to the next line
            $this->Ln($h);
        }

        function CheckPageBreak($h)
        {
            //If the height h would cause an overflow, add a new page immediately
            if($this->GetY()+$h>$this->PageBreakTrigger)
                $this->AddPage($this->CurOrientation);
        }

        function NbLines($w, $txt)
        {
            //Computes the number of lines a MultiCell of width w will take
            $cw=&$this->CurrentFont['cw'];
            if($w==0)
                $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r", '', $txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
            while($i<$nb)
            {
                $c=$s[$i];
                if($c=="\n")
                {
                    $i++;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                    continue;
                }
                if($c==' ')
                    $sep=$i;
                $l+=$cw[$c];
                if($l>$wmax)
                {
                    if($sep==-1)
                    {
                        if($i==$j)
                            $i++;
                    }
                    else
                        $i=$sep+1;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                }
                else
                    $i++;
            }
            return $nl;
        }
    }
?>