<?php
	if (!isset($_POST["accion"]))
	{
		echo json_encode(array("status" => "EPALE!"));
		exit;
	}

	session_name("agenda_sia_2015");
	session_start();
	$id_usuario_alcalde = "2";

	// CONEXION A LA BASE DE DATOS EN POSTGRES
	$pdo = new PDO("mysql:host=localhost;dbname=agenda_altamira;charset=utf8", "agenda_altamira", "=1ZX$1-p");

	if ( isset($_SESSION["usuario"]) )
	{
		$id_usuario = $_SESSION["usuario"]["id"];
		$query = $pdo->query("SELECT bloqueado FROM usuarios WHERE id_usuario = $id_usuario");
		$bloqueado = $query->fetchColumn();

		if ($bloqueado)
		{
			unset($_SESSION['usuario']);
			session_unset();
			session_destroy();
			session_write_close();
			
			echo json_encode(array("status" => "OK"));
		}
	}

	$accion = $_POST["accion"];

	switch ($accion)
	{
		case "iniciar-sesion":
			$usuario = $_POST["usuario"];
			$password = $_POST["password"];

			$query = $pdo->prepare("SELECT id_usuario AS id,
				u.id_nivel_usuario AS id_nivel, nu.nombre AS nivel,
				CONCAT(u.nombre, ' ', u.paterno) AS nombre,
				u.id_grupo_convocado AS id_grupo, gc.nombre AS nombre_grupo,
				u.bloqueado AS u_bloqueado, nu.bloqueado AS nu_bloqueado
				FROM usuarios u
				INNER JOIN niveles_usuarios nu ON u.id_nivel_usuario = nu.id_nivel_usuario
				INNER JOIN grupos_convocados gc ON gc.id_grupo_convocado = u.id_grupo_convocado
				WHERE usuario = :usuario AND BINARY password = :password AND u.borrado = 0 AND nu.borrado = 0");
			$query->execute(array("usuario" => $usuario, "password" => $password));
			$info_usuario = $query->fetch(PDO::FETCH_ASSOC);

			if ( !$info_usuario )
			{
				header("Location: ../index.php?e=1");
				exit;
			}
			else if ( $info_usuario["u_bloqueado"] === "1" || $info_usuario["nu_bloqueado"] === "1" )
			{
				header("Location: ../index.php?e=2");
				exit;
			}

			$id_usuario = $info_usuario["id"];
			$id_nivel = (int) $info_usuario["id_nivel"];
			$nivel = $info_usuario["nivel"];
			$nombre = $info_usuario["nombre"];
			$id_grupo = $info_usuario["id_grupo"];
			$nombre_grupo = $info_usuario["nombre_grupo"];

			$query = $pdo->query("SELECT au.clave, au.nombre AS acceso FROM permisos_usuarios pu
				INNER JOIN accesos_usuarios au ON au.id_acceso = pu.id_acceso
				WHERE id_nivel_usuario = $id_nivel AND pu.borrado = 0 AND au.borrado = 0");
			$permisos = $query->fetchAll(PDO::FETCH_ASSOC);

			$claves = array();
			for ($i = 0; $i < count($permisos); $i++)
			{
				array_push($claves, $permisos[$i]["clave"]);
			}

			// $query = $pdo->query("SELECT pa.id_agenda AS id, a.nombre FROM permisos_agendas pa
			// 	INNER JOIN agendas a ON a.id_agenda = pa.id_agenda
			// 	WHERE id_usuario = $id_usuario AND pa.borrado = 0 AND a.borrado = 0");
			// $agendas = $query->fetchAll(PDO::FETCH_ASSOC);

			// $eventos = array();
			// for ($i = 0; $i < count($agendas); $i++)
			// {
			// 	$query = $pdo->query("SELECT id_evento AS id, titulo, id_agenda AS agenda FROM eventos WHERE id_agenda = " . $agendas[$i]["id"] . " AND borrado = 0");
			// 	array_push($eventos, $query->fetchAll(PDO::FETCH_ASSOC));
			// }

			// session_name("agenda_sia_2015");
			// session_start();
			$_SESSION["usuario"]["id"] = $id_usuario;
			$_SESSION["usuario"]["nombre"] = $nombre;
			$_SESSION["usuario"]["grupo"] = array("id" => $id_grupo, "nombre" => $nombre_grupo);
			$_SESSION["usuario"]["nivel"] = $id_nivel;
			$_SESSION["usuario"]["permisos"] = $permisos;
			$_SESSION["usuario"]["claves"] = $claves;
			// $_SESSION["usuario"]["agendas"] = $agendas;
			// $_SESSION["usuario"]["eventos"] = $eventos;

			if ($id_nivel !== 1)
			{
				// Bitacora
				$accion = "Inició sesión.";
				$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, 0, '$accion', '', (SELECT NOW()))");
			}

			if (!in_array("0001", $claves) && in_array("4001", $claves)) // Solo lectura.
			{
				header("Location: ../web/r/");
				exit;
			}

			header("Location: ../web/");
			exit;
		break;

		case "cerrar-sesion":
			$id_nivel = (int) $_SESSION["usuario"]["nivel"];

			if ($id_nivel !== 1)
			{
				// Bitacora
				$id_usuario = $_SESSION["usuario"]["id"];
				$accion = "Cerró sesión.";
				$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, 0, '$accion', '', (SELECT NOW()))");
			}

			unset($_SESSION['usuario']);
			session_unset();
			session_destroy();
			session_write_close();
			
			echo json_encode(array("status" => "OK"));
		break;

		case "obtener-agendas":
			$id_usuario = $_SESSION["usuario"]["id"];
			$id_nivel_usuario = $_SESSION["usuario"]["nivel"];

			$query = $pdo->query("SELECT pa.id_agenda AS id, a.nombre
				FROM (SELECT * FROM permisos_agendas WHERE id_nivel_usuario = $id_nivel_usuario AND borrado = 0) pa
				INNER JOIN (SELECT * FROM agendas WHERE borrado = 0) a ON a.id_agenda = pa.id_agenda");
			$agendas = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $agendas));
		break;

		case "obtener-grupos-convocados":
			$query = $pdo->query("SELECT id_grupo_convocado AS id, nombre
				FROM grupos_convocados WHERE borrado = 0");
			$grupos_convocados = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $grupos_convocados));
		break;

		case "obtener-convocados":
			$query = $pdo->query("SELECT id_convocado AS id, id_grupo_convocado AS id_grupo, UPPER(CONCAT(nombre, ' ', paterno, ' ', materno)) AS nombre
				FROM convocados c INNER JOIN (SELECT * FROM personas WHERE borrado = 0) p ON p.id_persona = c.id_persona WHERE c.borrado = 0");
			$convocados = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $convocados));
		break;

		case "obtener-evento":
			$id_evento = $_POST["id"];

			$query = $pdo->query("SELECT titulo, descripcion, responsable, vestimenta, logistica,
				DATE_FORMAT(fecha_inicio, '%d/%m/%Y') AS fecha_inicio, DATE_FORMAT(hora_inicio, '%H:%i') AS hora_inicio,
				DATE_FORMAT(fecha_termino, '%d/%m/%Y') AS fecha_termino, DATE_FORMAT(hora_termino, '%H:%i') AS hora_termino,
				DATE_FORMAT(hora_llegada, '%H:%i') AS hora_llegada, DATE_FORMAT(hora_salida, '%H:%i') AS hora_salida,
				ubicacion_dir, ubicacion_lat, ubicacion_lng, publicado, cancelado
				FROM eventos WHERE id_evento = $id_evento AND borrado = 0");
			$evento = $query->fetch(PDO::FETCH_ASSOC);

			$query = $pdo->query("SELECT id_grupo_convocado AS id FROM grupos_convocados_eventos WHERE id_evento = $id_evento AND borrado = 0");
			$grupos_convocados_eventos = $query->fetchAll(PDO::FETCH_ASSOC);
			$convocados = array(
				"alcalde" => "0",
				"cabildo" => "0",
				"secretarios" => "0",
				"directores" => "0",
				"jefes" => "0",
				"dif" => "0"
			);

			for ($i = 0; $i < count($grupos_convocados_eventos); $i++)
			{
				$gce_id = (int) $grupos_convocados_eventos[$i]["id"];
				switch ($gce_id)
				{
					case 2: $convocados["alcalde"] = "1"; break;
					case 3: $convocados["cabildo"] = "1"; break;
					case 4: $convocados["secretarios"] = "1"; break;
					case 5: $convocados["directores"] = "1"; break;
					case 6: $convocados["jefes"] = "1"; break;
					case 7: $convocados["dif"] = "1"; break;
				}
			}

			$evento["convocados"] = $convocados;

			echo json_encode(array("status" => "OK", "resultado" => $evento));
		break;

		case "obtener-eventos":
			$id_usuario = $_SESSION["usuario"]["id"];
			$id_nivel_usuario = $_SESSION["usuario"]["nivel"];

			$query = $pdo->query("SELECT e.id_evento AS id, titulo, e.id_agenda AS agenda
				FROM (SELECT * FROM eventos WHERE borrado = 0) e
				INNER JOIN (SELECT * FROM permisos_agendas WHERE id_nivel_usuario = $id_nivel_usuario AND borrado = 0) pa ON pa.id_agenda = e.id_agenda");
			$eventos = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $eventos));
		break;

		case "obtener-eventos-dia":
			$id_usuario = $_SESSION["usuario"]["id"];
			$claves_usuario = $_SESSION["usuario"]["claves"];
			$id_grupo_convocado = $_SESSION["usuario"]["grupo"]["id"];

			$where_grupo_convocado = "";
			if ($id_grupo_convocado !== "1")
			{
				$where_grupo_convocado .= " AND e.id_evento IN (SELECT id_evento FROM grupos_convocados_eventos WHERE id_grupo_convocado = $id_grupo_convocado AND borrado = 0)";
			}

			if (!in_array("0001", $claves_usuario) && !in_array("0002", $claves_usuario))
			{
				$where_grupo_convocado .= " AND e.publicado = 1";
			}

			$dia = (int) $_POST["dia"];
			$mes = (int) $_POST["mes"];
			$temporada = (int) $_POST["temporada"];

			$fecha = $temporada . "-" . $mes . "-" . $dia;
			//$fecha_termino = $temporada . "-" . $mes . "-" . $dia;

			$query = $pdo->query("SELECT e.id_evento, titulo, IF(descripcion='','<em>Sin descripción.</em>', descripcion) AS descripcion,
				IF(responsable='','&#8722;',responsable) AS responsable, IF(vestimenta='','&#8722;',vestimenta) AS vestimenta, IF(logistica=0,'No','Si') AS logistica,
				IF((SELECT asistencia FROM asistencias WHERE id_usuario = 2 AND id_evento = e.id_evento AND borrado = 0)=$id_usuario_alcalde,'El alcalde SI asistirá.',
					IF(representante_alcalde!='',CONCAT('El alcalde NO asistirá. En cambio, ira ',representante_alcalde,' en su representación.'), 'El alcalde NO asistirá.')) AS representante,
				DATE_FORMAT(fecha_inicio, '%d/%m/%Y') AS fecha_inicio,
				DATE_FORMAT(hora_inicio, '%H:%i') AS hora_inicio,
				UNIX_TIMESTAMP(CONCAT(fecha_inicio,' ',hora_inicio)) AS unix_inicio,
				DATE_FORMAT(fecha_termino, '%d/%m/%Y') AS fecha_termino,
				DATE_FORMAT(hora_termino, '%H:%i') AS hora_termino,
				UNIX_TIMESTAMP(CONCAT(fecha_termino,' ',hora_termino)) AS unix_termino,
				DATE_FORMAT(hora_llegada, '%H:%i') AS hora_llegada, DATE_FORMAT(hora_salida, '%H:%i') AS hora_salida,
				ubicacion_dir, ubicacion_lat, ubicacion_lng,
				IF(publicado=0,'No','Si') AS publicado,
				CONCAT(u.nombre, ' ', u.paterno) AS creador,
				(SELECT asistencia FROM asistencias WHERE id_usuario = $id_usuario AND id_evento = e.id_evento AND borrado = 0) AS asistencia
				FROM (SELECT * FROM eventos WHERE borrado = 0) e
				INNER JOIN usuarios u ON u.id_usuario = e.id_usuario
				WHERE ('$fecha' between fecha_inicio AND fecha_termino OR fecha_inicio = '$fecha' OR fecha_termino = '$fecha') $where_grupo_convocado
				ORDER BY fecha_inicio, hora_inicio");
			$eventos = $query->fetchAll(PDO::FETCH_ASSOC);

			// Adicionar los convocados de cada evento
			for ($i = 0; $i < count($eventos); $i++)
			{
				$id_evento = $eventos[$i]["id_evento"];
				$query = $pdo->query("SELECT gc.nombre AS nombre FROM grupos_convocados_eventos gce
					INNER JOIN grupos_convocados gc ON gc.id_grupo_convocado = gce.id_grupo_convocado WHERE gce.id_evento = $id_evento AND gce.borrado = 0");
				$convocados = $query->fetchAll(PDO::FETCH_ASSOC);
				$eventos[$i]["convocados"] = $convocados;
			}

			echo json_encode(array("status" => "OK", "resultado" => $eventos));
		break;

		case "obtener-eventos-mes":
			$id_usuario = $_SESSION["usuario"]["id"];
			$claves_usuario = $_SESSION["usuario"]["claves"];
			$id_grupo_convocado = $_SESSION["usuario"]["grupo"]["id"];

			$where_grupo_convocado = "";
			if ($id_grupo_convocado !== "1")
			{
				$where_grupo_convocado .= " AND e.id_evento IN (SELECT id_evento FROM grupos_convocados_eventos WHERE id_grupo_convocado = $id_grupo_convocado AND borrado = 0)";
			}

			if (!in_array("0001", $claves_usuario) && !in_array("0002", $claves_usuario))
			{
				$where_grupo_convocado .= " AND e.publicado = 1";
			}

			$mes = (int) $_POST["mes"];
			$temporada = (int) $_POST["temporada"];
			$rango_de_dias = 7;

			$mes_anterior = $mes === 1 ? 12 : $mes - 1;
			$temporada_anterior = $mes_anterior === 12 ? $temporada - 1 : $temporada;
			$dias_en_mes_anterior = cal_days_in_month(CAL_GREGORIAN, $mes_anterior, $temporada_anterior);
			$dia_anterior = $dias_en_mes_anterior - $rango_de_dias;

			$mes_posterior = $mes === 12 ? 1 : $mes + 1;
			$temporada_posterior = $mes === 1 ? $temporada + 1 : $temporada;
			$dias_en_mes_posterior = cal_days_in_month(CAL_GREGORIAN, $mes_posterior, $temporada_posterior);
			$dia_posterior = $rango_de_dias;//$dias_en_mes_posterior - $rango_de_dias;

			// $dias_en_mes = cal_days_in_month(CAL_GREGORIAN, $mes, $temporada);
			$fecha_inicio = $temporada_anterior . "-" . $mes_anterior . "-" . $dia_anterior . " 00:00:00";
			$fecha_termino = $temporada_posterior . "-" . $mes_posterior . "-" . $dia_posterior . " 23:59:59";

			$query = $pdo->query("SELECT DATE_FORMAT(fecha_inicio, '%e%c%Y') AS fecha_inicio,
				DATE_FORMAT(hora_inicio, '%H:%i') AS hora_inicio,
				DATE_FORMAT(fecha_termino, '%e%c%Y') AS fecha_termino,
				DATE_FORMAT(hora_termino, '%H:%i') AS hora_termino
				FROM (SELECT * FROM eventos WHERE borrado = 0) e
				INNER JOIN usuarios u ON u.id_usuario = e.id_usuario
				WHERE (fecha_inicio between '$fecha_inicio' AND '$fecha_termino' OR fecha_termino between '$fecha_inicio' AND '$fecha_termino') $where_grupo_convocado
				ORDER BY fecha_inicio, hora_inicio");
			$eventos = $query->fetchAll(PDO::FETCH_ASSOC);

			echo json_encode(array("status" => "OK", "resultado" => $eventos));
		break;

		case "descargar-eventos":
			$eventos = json_decode($_POST["eventos"]);
			$inicio = preg_split("/\//", $_POST["inicio"], -1); // dd/mm/yyyy hh:mm:ss
			$dia_inicio = $inicio[0];
			$mes_inicio = $inicio[1];
			$tem_inicio = $inicio[2];

			$fecha_inicio = $tem_inicio . "-" . $mes_inicio . "-" . $dia_inicio;
			$dia_de_la_semana = date("w", strtotime($fecha_inicio));
			$dias_de_la_semana = array("Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado");

			$meses = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");

			//require_once("fpdf/plugins/pdf-mc-table.php");
			require_once("fpdf/fpdf.php");
			header('Content-type: application/pdf');

			$pdf = new FPDF("P", "mm", "Letter"); //Portrait, milimeters, letter size..

			$encabezado = "Agenda para el día " . $dias_de_la_semana[$dia_de_la_semana] . ", " . $dia_inicio . " de " .
            	$meses[ ((int)$mes_inicio) - 1 ] . " del " . $tem_inicio;
			$pdf->AddPage();
			$pdf->SetAutoPageBreak(true, 0.5);

			// Cabecera
            $pdf->Image("../css/img/cabecera.png",0,0,$pdf->w);
            $pdf->SetFont("Arial","",14);
            $pdf->SetXY(($pdf->w * 0.7)/3, 10);
            $pdf->MultiCell(($pdf->w * 1.65)/3,5,utf8_decode($encabezado),0,'C');

            // Cuerpo
            $cabecera_height = 32; // mm
            $evento_height = 61; // mm
            $limit_per_page = 4;
            $count = 0;

            for ($i = 0; $i < count($eventos); $i++)
			{
				$id_evento = $eventos[$i];

				$query = $pdo->query("SELECT DATE_FORMAT(hora_inicio, '%H:%i') AS hora_inicio,
					titulo, descripcion, responsable, vestimenta,
					ubicacion_dir AS ubicacion, DATE_FORMAT(hora_llegada, '%H:%i') AS hora_llegada,
					DATE_FORMAT(hora_salida, '%H:%i') AS hora_salida
					FROM eventos WHERE id_evento = $id_evento AND borrado = 0");
				$evento = $query->fetch(PDO::FETCH_ASSOC);

				$query = $pdo->query("SELECT id_grupo_convocado FROM grupos_convocados_eventos WHERE id_evento = $id_evento");
				$convocados = $query->fetchAll(PDO::FETCH_ASSOC);

				if ($count === $limit_per_page)
				{
					$pdf->AddPage();
					// Cabecera
		            $pdf->Image("../css/img/cabecera.png",0,0,$pdf->w);
		            $pdf->SetFont("Arial","",14);
		            $pdf->SetXY(($pdf->w * 0.7)/3, 10);
		            $pdf->MultiCell(($pdf->w * 1.65)/3,5,utf8_decode($encabezado),0,'C');
					$count = 0;
				}

				// Posicion para la plantilla y su contenido.
				$y = $cabecera_height + ($evento_height * $count);

				// Plantilla
				$pdf->Image("../css/img/plantilla.png",0,$y,$pdf->w);

				// Plantilla -> Hora Inicio
				$y_hora_inicio = $y + 9;

				$pdf->SetFont("Arial","B",14);
				$pdf->SetTextColor(255,255,255);
            	$pdf->SetXY(8.5, $y_hora_inicio);
            	$pdf->Cell(0,0,utf8_decode($evento["hora_inicio"]),0,0,'L');

            	/** LO QUE SE TIENE QUE ADAPTAR AUTOMATICAMENTE
            	/ - TITULO
            	/ - DIRECCION
            	*/

            	// Plantilla -> Titulo
            	$y_titulo = $y + 5;

            	$pdf->SetFont("Arial","B",12);
				$pdf->SetTextColor(100,100,100);
            	$pdf->SetXY(28, $y_titulo);
            	$pdf->MultiCell($pdf->w/2,4,utf8_decode($evento["titulo"]),0,'L');

            	// Plantilla -> Ubicacion
            	$pdf->SetFont("Arial","B",9);
				$pdf->SetTextColor(0,0,0);
            	$pdf->SetX(35);
            	$pdf->MultiCell($pdf->w/2,3.5,utf8_decode($evento["ubicacion"]),0,'L');

            	/** LO QUE SE TIENE QUE POSICIONAR DE FORMA ESTATICA
            	/ - RESPONSABLE
            	/ - CONVOCADOS
            	/ - HORA DE LLEGADA
            	/ - HORA DE SALIDA
            	/ - VESTIMENTA
            	/ - OTRO DATO (DESCRIPCION)
            	*/

            	// Plantilla -> Hora de Llegada
            	$y_llegada = $y + 20;

            	$pdf->SetFont("Arial","B",11);
            	$pdf->SetXY(15, $y_llegada);
            	$pdf->Cell(0,0,$evento["hora_llegada"],0,0,'L');

            	// Plantilla -> Hora de Salida
            	$y_llegada = $y + 25;

            	$pdf->SetFont("Arial","B",11);
            	$pdf->SetXY(15, $y_llegada);
            	$pdf->Cell(0,0,$evento["hora_salida"],0,0,'L');

            	// Plantilla -> Vestimenta
            	$y_vestimenta = $y + 32;

            	$pdf->SetFont("Arial","B",10);
            	$pdf->SetXY(30, $y_vestimenta);
            	$pdf->Cell(0,0,utf8_decode($evento["vestimenta"]),0,0,'L');

            	// Plantilla -> Otro dato (Descripcion)
            	$y_descripcion = $y + 47;

            	$pdf->SetFont("Arial","B",9.5);
            	$pdf->SetXY(30, $y_descripcion);
            	$pdf->MultiCell($pdf->w/1.8,3.5,utf8_decode($evento["descripcion"]),0,'L');

            	// Plantilla -> Responsable
            	$y_responsable = $y + 8;

            	$pdf->SetFont("Arial","B",9.5);
            	$pdf->SetXY($pdf->w/1.4, $y_responsable);
            	$pdf->MultiCell($pdf->w/3.75,3.5,utf8_decode($evento["responsable"]),0,'C');

            	// Plantilla -> Convocados
            	for ($j= 0; $j < count($convocados); $j++)
            	{
            		$id_grupo_convocado = (int) $convocados[$j]["id_grupo_convocado"];
            		$x_convocado = $pdf->w/1.3545;
            		$y_plus = 4.60;

            		switch ($id_grupo_convocado)
            		{
            			case 2:
            				// ALCALDE
            				$y_alcalde = $y + 34.25;

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_alcalde, "x");
            			break;
            			case 3:
            				// CABILDO
            				$y_cabildo = $y + 34.25 + ($y_plus*2);

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_cabildo, "x");
            			break;
            			case 4:
            				// SECRETARIOS
            				$y_secretarios = $y + 34.25 + ($y_plus*3);

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_secretarios, "x");
            			break;
            			case 5:
            				// DIRECTORES
            				$y_directores = $y + 34.25 + ($y_plus*4);

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_directores, "x");
            			break;
            			case 6:
            				// JEFES
            				$y_jefes = $y + 34.25 + ($y_plus*5);

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_jefes, "x");
            			break;
            			case 7:
            				// DIF
            				$y_dif = $y + 34.25 + ($y_plus); // esta antes de cabildo -_-

			            	$pdf->SetFont("Arial","B",10);
			            	$pdf->Text($x_convocado, $y_dif, "x");
            			break;
            		}
            	}

            	// $y_responsable = $y + 8;

            	// $pdf->SetFont("Arial","B",9.5);
            	// $pdf->SetXY($pdf->w/1.4, $y_responsable);
            	// $pdf->MultiCell($pdf->w/3.75,3.5,utf8_decode($evento["responsable"]),1,'C');

				$count += 1;
			}

			// Bitacora
			$id_usuario = $_SESSION["usuario"]["id"];
			$accion = "Descargó la agenda del ".$_POST["inicio"].".";
			$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, 0, '$accion', '', (SELECT NOW()))");

            $pdf->Output($encabezado . ".pdf", "D");
		break;

		case "crear-evento":
			$id_usuario = $_SESSION["usuario"]["id"];
			$titulo = $_POST["titulo"];
			$descripcion = $_POST["descripcion"];
			$responsable = $_POST["responsable"];
			$vestimenta  = $_POST["vestimenta"];
			$logistica   = $_POST["logistica"] === "true" ? "1" : "0";
			$convocados = array(
				"alcalde" => $_POST["convocados_alcalde"],
				"cabildo" => $_POST["convocados_cabildo"],
				"secretarios" => $_POST["convocados_secretarios"],
				"directores" => $_POST["convocados_directores"],
				"jefes" => $_POST["convocados_jefes"],
				"dif" => $_POST["convocados_dif"]
			);
			$publicado = $_POST["publicado"] === "true" ? "1" : "0";
			$fecha_inicio = $_POST["fecha_inicio"];
			$hora_inicio  = $_POST["hora_inicio"];
			$fecha_termino = $_POST["fecha_termino"];
			$hora_termino  = $_POST["hora_termino"];
			$hora_llegada  = $_POST["hora_llegada"];
			$hora_salida = $_POST["hora_salida"];
			$ubicacion_dir = $_POST["ubicacion_dir"];
			$ubicacion_lat = $_POST["ubicacion_lat"];
			$ubicacion_lng = $_POST["ubicacion_lng"];
			$codigo = $_POST["codigo"];
			$existe_codigo = 0;

			if ($publicado === "1")
			{
				$query = $pdo->query("SELECT count(*) FROM codigos WHERE codigo = '$codigo'");
				$existe_codigo = $query->fetchColumn();

				if (!$existe_codigo)
				{
					echo json_encode(array("status" => "CODE ERROR"));
					exit;
				}
			}
			// $cancelado = isset($_POST["cancelar-evento"]) ? "1" : "0";
			// $borrado = isset($_POST["borrar-evento"]) ? "1" : "0";

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicio, -1); // dd/mm/yyyy hh:mm:ss
			$dia_inicio = $fi[0];
			$mes_inicio = $fi[1];
			$tem_inicio = $fi[2];

			$fecha_inicio = $tem_inicio . "-" . $mes_inicio . "-" . $dia_inicio;
			$fecha_hora_inicio = $fecha_inicio . " " . $hora_inicio;
			$unix_inicio = strtotime($fecha_hora_inicio);

			$unix_hoy = time();

			// Revisar si es una fecha pasada a comparacion de este momento.
			if ($unix_hoy > $unix_inicio)
			{
				echo json_encode(array("status" => "PASSED DATE"));
				exit;
			}

			// Empiezan validaciones para los que crean eventos
			$fecha_hoy = date("Y-m-d");
			if ($fecha_hoy === $fecha_inicio)
			{
				echo json_encode(array("status" => "NOT TODAY"));
				exit;
			}

			$fecha_dia_siguiente = date("Y-m-d", strtotime("+1 day"));
			if ($fecha_dia_siguiente === $fecha_inicio)
			{
				$hora_limite = 14;
				$unix_limite = mktime($hora_limite,0,0,date("m"),date("d"),date("Y"));

				if ($unix_hoy > $unix_limite)
				{
					echo json_encode(array("status" => "NOT TOMORROW"));
					exit;
				}
			}
			// Terminan validaciones.

			if ($fecha_termino !== "")
			{
				$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
				$dia_termino = $ft[0];
				$mes_termino = $ft[1];
				$tem_termino = $ft[2];

				$fecha_termino = $tem_termino . "-" . $mes_termino . "-" . $dia_termino;
			}
			else
			{
				$dia_termino = $dia_inicio;
				$mes_termino = $mes_inicio;
				$tem_termino = $tem_inicio;

				$fecha_termino = $fecha_inicio;
			}

			if ($hora_termino === "")
			{
				$hora_termino = $hora_inicio;
			}

			$fecha_hora_termino = $fecha_termino . " " . $hora_termino;
			$unix_termino = strtotime($fecha_hora_termino);

			if ($unix_inicio > $unix_termino)
			{
				echo json_encode(array("status" => "DATE RANGE ERROR"));
				exit;
			}

			$query = $pdo->query("SELECT count(*) FROM eventos
				WHERE (UNIX_TIMESTAMP(CONCAT(fecha_inicio,' ',hora_inicio)) between '$unix_inicio' AND '$unix_termino'
					OR UNIX_TIMESTAMP(CONCAT(fecha_termino,' ',hora_termino)) between '$unix_inicio' AND '$unix_termino') AND borrado = 0");
			$ya_existe_evento = $query->fetchColumn();

			if ($ya_existe_evento > 0)
			{
				echo json_encode(array("status" => "DUPLICATED"));
				exit;
			}
			else
			{
				$insert = $pdo->query("INSERT INTO eventos (titulo, descripcion, responsable, vestimenta, logistica, fecha_inicio, hora_inicio, fecha_termino, hora_termino, hora_llegada, hora_salida, ubicacion_dir, ubicacion_lat, ubicacion_lng, id_usuario, fecha_creacion, publicado)
					VALUES ('$titulo', '$descripcion', '$responsable', '$vestimenta', '$logistica', '$fecha_inicio', '$hora_inicio', '$fecha_termino', '$hora_termino', '$hora_llegada', '$hora_salida', '$ubicacion_dir', $ubicacion_lat, $ubicacion_lng, $id_usuario, (SELECT NOW()), $publicado)");
				if ($insert)
				{
					$id_evento = $pdo->lastInsertId();

					if ($convocados["alcalde"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 2, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["cabildo"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 3, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["secretarios"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 4, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["directores"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 5, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["jefes"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 6, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["dif"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 7, $id_evento, (SELECT NOW()), 0)");
					}

					// Bitacora
					$accion = "Creó el evento.";
					$json = json_encode($_POST);
					$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

					if ($publicado === "1" && $existe_codigo)
					{
						// Bitacora
						$accion = "Publicó el evento.";
						$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '', (SELECT NOW()))");
					}

					echo json_encode(array("status" => "OK", "resultado" => array("dia_inicio" => $dia_inicio, "mes_inicio" => $mes_inicio, "temporada_inicio" => $tem_inicio,"dia_termino" => $dia_termino, "mes_termino" => $mes_termino, "temporada_termino" => $tem_termino)));
					exit;
				}
				else
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}
			}
		break;

		case "guardar-cambios-evento":
			$id_usuario = $_SESSION["usuario"]["id"];
			$id_evento = $_POST["id"];
			$titulo = $_POST["titulo"];
			$descripcion = $_POST["descripcion"];
			$responsable = $_POST["responsable"];
			$vestimenta  = $_POST["vestimenta"];
			$logistica   = $_POST["logistica"] === "true" ? "1" : "0";
			$convocados = array(
				"alcalde" => $_POST["convocados_alcalde"],
				"cabildo" => $_POST["convocados_cabildo"],
				"secretarios" => $_POST["convocados_secretarios"],
				"directores" => $_POST["convocados_directores"],
				"jefes" => $_POST["convocados_jefes"],
				"dif" => $_POST["convocados_dif"]
			);
			$publicado = $_POST["publicado"] === "true" ? "1" : "0";
			$fecha_inicio = $_POST["fecha_inicio"];
			$hora_inicio  = $_POST["hora_inicio"];
			$fecha_termino = $_POST["fecha_termino"];
			$hora_termino  = $_POST["hora_termino"];
			$hora_llegada  = $_POST["hora_llegada"];
			$hora_salida = $_POST["hora_salida"];
			$ubicacion_dir = $_POST["ubicacion_dir"];
			$ubicacion_lat = $_POST["ubicacion_lat"];
			$ubicacion_lng = $_POST["ubicacion_lng"];
			$codigo = $_POST["codigo"];
			// $cancelado = isset($_POST["cancelar-evento"]) ? "1" : "0";
			// $borrado = isset($_POST["borrar-evento"]) ? "1" : "0";
			$existe_codigo = 0;

			// Checar si ya fue publicado el evento
			$query = $pdo->query("SELECT count(*) FROM eventos WHERE id_evento = $id_evento AND publicado = 1");
			$ya_fue_publicado = $query->fetchColumn();

			if ($publicado === "1")
			{
				$query = $pdo->query("SELECT count(*) FROM codigos WHERE codigo = '$codigo'");
				$existe_codigo = $query->fetchColumn();

				if (!$existe_codigo)
				{
					echo json_encode(array("status" => "CODE ERROR"));
					exit;
				}
			}

			// DAR FORMATO A LA FECHA PARA MYSQL
			$fi = preg_split("/\//", $fecha_inicio, -1); // dd/mm/yyyy hh:mm:ss
			$dia_inicio = $fi[0];
			$mes_inicio = $fi[1];
			$tem_inicio = $fi[2];

			$fecha_inicio = $tem_inicio . "-" . $mes_inicio . "-" . $dia_inicio;
			$fecha_hora_inicio = $fecha_inicio . " " . $hora_inicio;
			$unix_inicio = strtotime($fecha_hora_inicio);

			$unix_hoy = time();

			if ($unix_hoy > $unix_inicio)
			{
				echo json_encode(array("status" => "PASSED DATE"));
				exit;
			}

			// Empiezan validaciones para los que editan eventos.
			$fecha_hoy = date("Y-m-d");
			if ($fecha_hoy === $fecha_inicio)
			{
				echo json_encode(array("status" => "NOT TODAY"));
				exit;
			}

			$fecha_dia_siguiente = date("Y-m-d", strtotime("+1 day"));
			if ($fecha_dia_siguiente === $fecha_inicio)
			{
				$hora_limite = 14;
				$unix_limite = mktime($hora_limite,0,0,date("m"),date("d"),date("Y"));

				if ($unix_hoy > $unix_limite)
				{
					echo json_encode(array("status" => "NOT TOMORROW"));
					exit;
				}
			}
			// Terminan validaciones.

			if ($fecha_termino !== "")
			{
				$ft = preg_split("/\//", $fecha_termino, -1); // dd/mm/yyyy hh:mm:ss
				$dia_termino = $ft[0];
				$mes_termino = $ft[1];
				$tem_termino = $ft[2];

				$fecha_termino = $tem_termino . "-" . $mes_termino . "-" . $dia_termino;
			}
			else
			{
				$dia_termino = $dia_inicio;
				$mes_termino = $mes_inicio;
				$tem_termino = $tem_inicio;

				$fecha_termino = $fecha_inicio;
			}

			if ($hora_termino === "")
			{
				$hora_termino = $hora_inicio;
			}

			$fecha_hora_termino = $fecha_termino . " " . $hora_termino;
			$unix_termino = strtotime($fecha_hora_termino);

			if ($unix_inicio > $unix_termino)
			{
				echo json_encode(array("status" => "DATE RANGE ERROR"));
				exit;
			}

			$query = $pdo->query("SELECT count(*) FROM eventos
				WHERE (UNIX_TIMESTAMP(CONCAT(fecha_inicio,' ',hora_inicio)) between '$unix_inicio' AND '$unix_termino'
					OR UNIX_TIMESTAMP(CONCAT(fecha_termino,' ',hora_termino)) between '$unix_inicio' AND '$unix_termino') AND id_evento != $id_evento AND borrado = 0");
			$ya_existe_evento = $query->fetchColumn();

			if ($ya_existe_evento > 0)
			{
				echo json_encode(array("status" => "DUPLICATED"));
				exit;
			}
			else
			{
				$update = $pdo->query("UPDATE eventos SET
						titulo = '$titulo',
						descripcion = '$descripcion',
						responsable = '$responsable',
						vestimenta = '$vestimenta',
						logistica = '$logistica',
						fecha_inicio = '$fecha_inicio',
						hora_inicio = '$hora_inicio',
						fecha_termino = '$fecha_termino',
						hora_termino = '$hora_termino',
						hora_llegada = '$hora_llegada',
						hora_salida = '$hora_salida',
						ubicacion_dir = '$ubicacion_dir',
						ubicacion_lat = $ubicacion_lat,
						ubicacion_lng = $ubicacion_lng,
						publicado = $publicado
					WHERE id_evento = $id_evento");
				if ($update)
				{

					// Proceso update de los grupos convocados que anteriormente ya habian sido convocados.
					$query = $pdo->query("SELECT id_grupo_convocado AS id, borrado FROM grupos_convocados_eventos WHERE id_evento = $id_evento ORDER BY id_grupo_convocado");
					$grupos_convocados_eventos = $query->fetchAll(PDO::FETCH_ASSOC);

					for ($i = 0; $i < count($grupos_convocados_eventos); $i++)
					{
						$gce_id = (int) $grupos_convocados_eventos[$i]["id"];
						$gce_borrado = $grupos_convocados_eventos[$i]["borrado"];

						switch ( $gce_id )
						{
							case 2:
								if ($convocados["alcalde"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 2");
								} else if ($convocados["alcalde"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 2");
								}

								$convocados["alcalde"] = "false";
							break;

							case 3:
								if ($convocados["cabildo"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 3");
								} else if ($convocados["cabildo"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 3");
								}

								$convocados["cabildo"] = "false";
							break;

							case 4:
								if ($convocados["secretarios"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 4");
								} else if ($convocados["secretarios"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 4");
								}

								$convocados["secretarios"] = "false";
							break;

							case 5:
								if ($convocados["directores"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 5");
								} else if ($convocados["directores"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 5");
								}

								$convocados["directores"] = "false";
							break;

							case 6:
								if ($convocados["jefes"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 6");
								} else if ($convocados["jefes"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 6");
								}

								$convocados["jefes"] = "false";
							break;

							case 7:
								if ($convocados["dif"] === "true" && $gce_borrado === "1") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 0 WHERE id_evento = $id_evento AND id_grupo_convocado = 7");
								} else if ($convocados["dif"] === "false" && $gce_borrado === "0") {
									$update = $pdo->query("UPDATE grupos_convocados_eventos SET borrado = 1 WHERE id_evento = $id_evento AND id_grupo_convocado = 7");
								}

								$convocados["dif"] = "false";
							break;
						}
					}

					// Proceso para los nuevos grupos que han sido convocados al evento.
					if ($convocados["alcalde"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 2, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["cabildo"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 3, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["secretarios"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 4, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["directores"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 5, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["jefes"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 6, $id_evento, (SELECT NOW()), 0)");
					}

					if ($convocados["dif"] === "true") {
						$insert = $pdo->query("INSERT INTO grupos_convocados_eventos VALUES (NULL, 7, $id_evento, (SELECT NOW()), 0)");
					}

					// Bitacora
					$accion = "Editó el evento.";
					$json = json_encode($_POST);
					$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

					if (!$ya_fue_publicado && $publicado === "1" && $existe_codigo)
					{
						// Bitacora
						$accion = "Publicó el evento.";
						$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '', (SELECT NOW()))");
					}
					else if ($ya_fue_publicado && $publicado === "0")
					{
						// Bitacora
						$accion = "Privó el evento.";
						$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '', (SELECT NOW()))");
					}

					echo json_encode(array("status" => "OK", "resultado" => array("dia_inicio" => $dia_inicio, "mes_inicio" => $mes_inicio, "temporada_inicio" => $tem_inicio,"dia_termino" => $dia_termino, "mes_termino" => $mes_termino, "temporada_termino" => $tem_termino)));
					exit;
				}
				else
				{
					echo json_encode(array("status" => "ERROR"));
					exit;
				}
			}
		break;

		case "eliminar-evento":
			$id_evento = $_POST["id"];
			$id_usuario = $_SESSION["usuario"]["id"];

			$query = $pdo->query("SELECT UNIX_TIMESTAMP(fecha_inicio) AS unix_inicio, publicado FROM eventos WHERE id_evento = $id_evento");
			$evento = $query->fetch(PDO::FETCH_ASSOC);
			$unix_inicio = $evento["unix_inicio"];
			$publicado = $evento["publicado"];

			$unix_hoy = time();

			if ($unix_hoy > $unix_inicio && $publicado === "1")
			{
				echo json_encode(array("status" => "PASSED DATE"));
				exit;
			}

			$update = $pdo->query("UPDATE eventos SET borrado = 1 WHERE id_evento = $id_evento");

			if ($update)
			{
				// Bitacora
				$accion = "Eliminó el evento.";
				$json = json_encode($_POST);
				$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

				$query = $pdo->query("SELECT DATE_FORMAT(fecha_inicio, '%m') AS mes_inicio, DATE_FORMAT(fecha_inicio, '%Y') AS temporada_inicio,
					DATE_FORMAT(fecha_termino, '%m') AS mes_termino, DATE_FORMAT(fecha_termino, '%Y') AS temporada_termino
					FROM eventos WHERE id_evento = $id_evento");
				$resultado = $query->fetch(PDO::FETCH_ASSOC);

				echo json_encode(array("status" => "OK", "resultado" => $resultado));
				exit;
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}
		break;

		case "mandar-representante-evento":
			$id_evento = $_POST["id"];
			$id_usuario = $_SESSION["usuario"]["id"];
			$representante = mb_strtoupper($_POST["representante"], "UTF-8");

			$update = $pdo->query("UPDATE eventos SET representante_alcalde = '$representante' WHERE id_evento = $id_evento");

			if ($update)
			{
				// Bitacora
				$accion = "Ingresó el representante del alcalde con el nombre de '$representante'.";
				$json = json_encode($_POST);
				$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

				echo json_encode(array("status" => "OK"));
				exit;
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}
		break;

		case "confirmar-asistencia":
			$id_evento = $_POST["id"];
			$id_usuario = $_SESSION["usuario"]["id"];
			$asistencia = $_POST["asistencia"] === "true" ? true : false;
			$es_alcalde = $id_usuario === $id_usuario_alcalde ? true : false;

			$update = $pdo->query("UPDATE asistencias SET borrado = 1 WHERE id_evento = $id_evento AND id_usuario = $id_usuario AND borrado = 0");

			if ($update)
			{
				if ($asistencia)
				{
					$insert = $pdo->query("INSERT INTO asistencias VALUES (NULL, $id_evento, $id_usuario, 1, (SELECT NOW()), 0)");

					if ($insert)
					{
						// Bitacora
						$accion = "Confirmó su asistencia al evento.";
						$json = json_encode($_POST);
						$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

						echo json_encode(array("status" => "OK", "resultado" => array("es_alcalde" => $es_alcalde, "asistencia" => $asistencia)));
						exit;
					}
				}
				else // Cancelar asistencia.
				{
					$insert = $pdo->query("INSERT INTO asistencias VALUES (NULL, $id_evento, $id_usuario, 0, (SELECT NOW()), 0)");

					if ($insert)
					{
						// Bitacora
						$accion = "Canceló su asistencia al evento.";
						$json = json_encode($_POST);
						$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, $id_evento, '$accion', '$json', (SELECT NOW()))");

						$query = $pdo->query("SELECT representante_alcalde FROM eventos WHERE id_evento = $id_evento");
						$representante = $query->fetchColumn();

						// echo json_encode(array("status" => "OK", "resultado" => array("representante" => $representante)));
						echo json_encode(array("status" => "OK", "resultado" => array("es_alcalde" => $es_alcalde, "asistencia" => $asistencia, "representante" => $representante)));
						exit;
					}
				}
			}
			else
			{
				echo json_encode(array("status" => "ERROR"));
				exit;
			}
			/*$query = $pdo->query("SELECT count(*) FROM asistencias WHERE id_evento = $id_evento AND id_usuario = $id_usuario");
			$existe_asistencia = $query->fetchColumn();

			if ($existe_asistencia)
			{
				if ($asistencia === "true")
				{
					$update = $pdo->query("UPDATE asistencias SET fecha_captura = (SELECT NOW()), borrado = 0 WHERE id_evento = $id_evento AND id_usuario = $id_usuario");
				}
				else
				{
					$update = $pdo->query("UPDATE asistencias SET fecha_captura = (SELECT NOW()), borrado = 1 WHERE id_evento = $id_evento AND id_usuario = $id_usuario");
				}
			}
			else
			{
				$insert = $pdo->query("INSERT INTO asistencias VALUES (NULL, $id_evento, $id_usuario, (SELECT NOW()), 0)");
			}*/
		break;

		case "vio-agenda":
			$id_usuario = $_SESSION["usuario"]["id"];
			
			$dia_agenda = $_POST["dia_agenda"];
			$mes_agenda = $_POST["mes_agenda"];
			$tem_agenda = $_POST["tem_agenda"];

			$fecha_agenda = $tem_agenda . "-" . $mes_agenda . "-" . $dia_agenda;

			$insert = $pdo->query("INSERT INTO historial_vistas VALUES (NULL, $id_usuario, '$fecha_agenda', (SELECT NOW()))");

			// Bitacora
			$accion = "Visualizó la agenda del ".$fecha_agenda.".";
			$insert = $pdo->query("INSERT INTO bitacora VALUES (NULL, $id_usuario, 0, '$accion', '', (SELECT NOW()))");
		break;

		case "obtener-historial-vistas":
		break;
	}
?>