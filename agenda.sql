-- phpMyAdmin SQL Dump
-- version 4.0.4.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-10-2015 a las 15:13:37
-- Versión del servidor: 5.6.11
-- Versión de PHP: 5.5.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `agenda`
--
CREATE DATABASE IF NOT EXISTS `agenda` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `agenda`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `accesos_usuarios`
--

CREATE TABLE IF NOT EXISTS `accesos_usuarios` (
  `id_acceso` int(11) NOT NULL AUTO_INCREMENT,
  `clave` int(4) unsigned zerofill NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `descripcion` varchar(250) NOT NULL,
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_acceso`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Volcado de datos para la tabla `accesos_usuarios`
--

INSERT INTO `accesos_usuarios` (`id_acceso`, `clave`, `nombre`, `descripcion`, `borrado`) VALUES
(1, 0001, 'Sudo', 'Super Usuario', 0),
(2, 1001, 'Acceder a Inicio.', 'Acceder al módulo de Inicio.', 0),
(3, 1002, 'Acceder a Bitacora.', 'Acceder al módulo de Bitacora.', 0),
(4, 1003, 'Acceder a Historial.', 'Acceder al módulo de Historial.', 0),
(5, 1004, 'Acceder a Asistencia.', 'Acceder al módulo de Asistencia.', 0),
(6, 2001, 'Editar un Evento', 'Acceder a la opción de Editar Evento', 0),
(7, 3001, 'Eliminar un Evento', 'Acceder a la opción de Eliminar Evento', 0),
(8, 2002, 'Mandar Representante', 'Acceder a la opción de mandar representante.', 0),
(9, 1005, 'Acceder a Impresión de Eventos.', 'Acceder al botón que te permite Imprimir los Eventos.', 0),
(10, 4001, 'Acceder a Solo Lectura.', 'Acceder a vista de solo lectura.', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencias`
--

CREATE TABLE IF NOT EXISTS `asistencias` (
  `id_asistencia` int(11) NOT NULL AUTO_INCREMENT,
  `id_evento` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `asistencia` tinyint(4) DEFAULT NULL,
  `fecha_captura` datetime NOT NULL,
  `borrado` tinyint(4) NOT NULL,
  PRIMARY KEY (`id_asistencia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora`
--

CREATE TABLE IF NOT EXISTS `bitacora` (
  `id_bitacora` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `accion` text NOT NULL,
  `json` text NOT NULL,
  `fecha_captura` datetime NOT NULL,
  PRIMARY KEY (`id_bitacora`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `bitacora`
--

INSERT INTO `bitacora` (`id_bitacora`, `id_usuario`, `id_evento`, `accion`, `json`, `fecha_captura`) VALUES
(1, 1, 0, 'El usuario admin editó el evento con el ID :2.', '{"agenda":"2","evento":"2","titulo":"Evento Secundario","descripcion":"Es un evento que se haru00e1 despuu00e9s del primero 1.","fecha-inicia":"2015-07-15 12:15","fecha-termina":"2015-07-15 16:00","ubicacion-dir":"Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, Mu00e9xico","ubicacion-lat":"22.392547","ubicacion-long":"-97.936807","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-10 12:19:46'),
(2, 1, 0, 'El usuario admin editó el evento con el ID :2', '{"agenda":"2","evento":"2","titulo":"Evento Secundario","descripcion":"Es un evento que se haru00e1 despuu00e9s del primero 1.","fecha-inicia":"2015-07-15 12:15","fecha-termina":"2015-07-15 16:00","ubicacion-dir":"Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, Mu00e9xico","ubicacion-lat":"22.392547","ubicacion-long":"-97.936807","cancelar-evento":"on","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-10 13:16:59'),
(3, 1, 0, 'El usuario admin editó el evento con el ID :2', '{"agenda":"2","evento":"2","titulo":"Evento Secundario","descripcion":"Es un evento que se haru00e1 despuu00e9s del primero 1.","fecha-inicia":"2015-07-15 12:15","fecha-termina":"2015-07-15 16:00","ubicacion-dir":"Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, Mu00e9xico","ubicacion-lat":"22.392547","ubicacion-long":"-97.936807","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-10 13:17:07'),
(4, 1, 0, 'El usuario admin editó el evento con el ID :2', '{"agenda":"2","evento":"2","titulo":"Evento Secundario","descripcion":"Es un evento que se haru00e1 despuu00e9s del primero 1.","fecha-inicia":"2015-07-15 12:15","fecha-termina":"2015-07-15 16:00","ubicacion-dir":"Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, Mu00e9xico","ubicacion-lat":"22.392547","ubicacion-long":"-97.936807","cancelar-evento":"on","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-10 13:17:47'),
(5, 1, 0, 'El usuario admin editó el evento con el ID :2', '{"agenda":"2","evento":"2","titulo":"Evento Secundario","descripcion":"Es un evento que se haru00e1 despuu00e9s del primero 1.","fecha-inicia":"2015-07-15 12:15","fecha-termina":"2015-07-15 16:00","ubicacion-dir":"Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, Mu00e9xico","ubicacion-lat":"22.392547","ubicacion-long":"-97.936807","cancelar-evento":"on","borrar-evento":"on","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-10 13:17:56'),
(6, 1, 0, 'El usuario admin editó el evento con el ID :9', '{"agenda":"1","evento":"9","titulo":"Evento A1","descripcion":"Es un evento especial llamado A1.","fecha-inicia":"2015-08-17 16:15","fecha-termina":"2015-08-17 18:00","ubicacion-dir":"Quintero, Zona Centro, Altamira, Mu00e9xico","ubicacion-lat":"0","ubicacion-lng":"0","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-17 17:16:12'),
(7, 1, 0, 'El usuario admin editó el evento con el ID :9', '{"agenda":"1","evento":"9","titulo":"Evento A1","descripcion":"Es un evento especial llamado A1.","fecha-inicia":"2015-08-17 16:15","fecha-termina":"2015-08-17 18:00","ubicacion-dir":"Quintero, Zona Centro, Altamira, Mu00e9xico","ubicacion-lat":"22.3917129","ubicacion-lng":"-97.93645749999996","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-17 17:17:04'),
(8, 1, 0, 'El usuario admin editó el evento con el ID :10', '{"agenda":"1","evento":"10","titulo":"Evento A2","descripcion":"Este es un","fecha-inicia":"2015-08-17 16:30","fecha-termina":"2015-08-17 17:45","ubicacion-dir":"Queru00e9taro, Mu00e9xico","ubicacion-lat":"20.5887932","ubicacion-lng":"-100.38988810000001","editar-evento":"Editar","accion":"accion-evento"}', '2015-08-17 17:17:12'),
(9, 1, 19, 'Creó el evento.', '{"accion":"crear-evento","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"","hora_termino":"","hora_llegada":"","hora_salida":"","ubicacion_dir":"calle","ubicacion_lat":"0","ubicacion_lng":"0","codigo":""}', '2015-10-19 08:55:05'),
(10, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"19/10/2015","hora_termino":"","hora_llegada":"00:00","hora_salida":"00:00","ubicacion_dir":"calle","ubicacion_lat":"0","ubicacion_lng":"0","codigo":""}', '2015-10-19 09:13:06'),
(11, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"","hora_termino":"10:00","hora_llegada":"00:00","hora_salida":"00:00","ubicacion_dir":"calle","ubicacion_lat":"0","ubicacion_lng":"0","codigo":""}', '2015-10-19 09:13:16'),
(12, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"19/10/2015","hora_termino":"11:00","hora_llegada":"00:00","hora_salida":"00:00","ubicacion_dir":"calle","ubicacion_lat":"0","ubicacion_lng":"0","codigo":""}', '2015-10-19 09:13:25'),
(13, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"19/10/2015","hora_termino":"11:00","hora_llegada":"00:00","hora_salida":"00:00","ubicacion_dir":"Altamira, Mu00e9xico","ubicacion_lat":"22.4077462","ubicacion_lng":"-97.9211555","codigo":""}', '2015-10-19 09:13:51'),
(14, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"19/10/2015","hora_termino":"11:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Altamira, Mu00e9xico","ubicacion_lat":"22.4077462","ubicacion_lng":"-97.9211555","codigo":""}', '2015-10-19 09:15:13'),
(15, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:00","fecha_termino":"19/10/2015","hora_termino":"11:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 09:23:42'),
(16, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"11:00","fecha_termino":"19/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 10:44:28'),
(17, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"19/10/2015","hora_inicio":"10:45","fecha_termino":"19/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 10:44:49'),
(18, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"20/10/2015","hora_inicio":"10:45","fecha_termino":"20/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 10:48:48'),
(19, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"21/10/2015","hora_inicio":"10:45","fecha_termino":"21/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 11:13:20'),
(20, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"20/10/2015","hora_inicio":"10:45","fecha_termino":"20/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 11:15:19'),
(21, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"20/10/2015","hora_inicio":"10:45","fecha_termino":"20/10/2015","hora_termino":"10:45","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 11:16:52'),
(22, 1, 19, 'Editó el evento.', '{"accion":"guardar-cambios-evento","id":"19","titulo":"a","descripcion":"","responsable":"","vestimenta":"","logistica":"false","convocados_alcalde":"false","convocados_cabildo":"false","convocados_secretarios":"false","convocados_directores":"false","convocados_jefes":"false","convocados_dif":"false","publicado":"false","fecha_inicio":"20/10/2015","hora_inicio":"10:45","fecha_termino":"20/10/2015","hora_termino":"12:00","hora_llegada":"10:15","hora_salida":"10:45","ubicacion_dir":"Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., Mu00e9xico","ubicacion_lat":"22.406108850686103","ubicacion_lng":"-97.9236102104187","codigo":""}', '2015-10-19 11:17:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `codigos`
--

CREATE TABLE IF NOT EXISTS `codigos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `codigos`
--

INSERT INTO `codigos` (`id`, `codigo`) VALUES
(1, 1112);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dt_asistencia`
--
CREATE TABLE IF NOT EXISTS `dt_asistencia` (
`id` int(11)
,`evento` varchar(350)
,`nombre` text
,`asistencia` varchar(8)
,`timestamp` varchar(21)
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `dt_historial`
--
CREATE TABLE IF NOT EXISTS `dt_historial` (
`id` int(11)
,`nombre` text
,`agenda` varchar(10)
,`timestamp` varchar(21)
);
-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eventos`
--

CREATE TABLE IF NOT EXISTS `eventos` (
  `id_evento` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(350) NOT NULL,
  `descripcion` text NOT NULL,
  `responsable` varchar(200) NOT NULL,
  `vestimenta` varchar(120) NOT NULL,
  `logistica` tinyint(4) NOT NULL DEFAULT '0',
  `representante_alcalde` varchar(200) CHARACTER SET utf8 NOT NULL,
  `fecha_inicio` date NOT NULL,
  `hora_inicio` time NOT NULL,
  `fecha_termino` date NOT NULL,
  `hora_termino` time NOT NULL,
  `hora_llegada` time NOT NULL,
  `hora_salida` time NOT NULL,
  `ubicacion_dir` varchar(500) NOT NULL,
  `ubicacion_lat` double NOT NULL,
  `ubicacion_lng` double NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `publicado` tinyint(4) NOT NULL DEFAULT '0',
  `cancelado` tinyint(4) NOT NULL DEFAULT '0',
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_evento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `eventos`
--

INSERT INTO `eventos` (`id_evento`, `titulo`, `descripcion`, `responsable`, `vestimenta`, `logistica`, `representante_alcalde`, `fecha_inicio`, `hora_inicio`, `fecha_termino`, `hora_termino`, `hora_llegada`, `hora_salida`, `ubicacion_dir`, `ubicacion_lat`, `ubicacion_lng`, `id_usuario`, `fecha_creacion`, `publicado`, `cancelado`, `borrado`) VALUES
(1, 'Evento Principal', 'El evento de inauguración que se llevara a cabo próximamente.', '', '', 0, '', '2015-07-15', '00:00:00', '2015-07-15', '00:00:00', '00:00:00', '00:00:00', 'Calle Quintero, Altamira, Tamaulipas, México', 22.39177, -97.937412, 1, '2015-07-14 11:26:38', 0, 0, 0),
(2, 'Evento Secundario', 'Es un evento que se hará después del primero 1.', '', '', 0, '', '2015-07-15', '00:00:00', '2015-07-15', '00:00:00', '00:00:00', '00:00:00', 'Calle Hidalgo 112 Zona Centro, Altamira, Tamaulipas, México', 22.392547, -97.936807, 1, '2015-07-14 14:01:39', 0, 1, 0),
(3, 'Evento Principal Parte 2', 'La segunda parte del evento principal.', '', '', 0, '', '2015-07-15', '00:00:00', '2015-07-15', '00:00:00', '00:00:00', '00:00:00', 'Calle Quintero, Altamira, Tamaulipas, México', 22.39177, -97.937412, 1, '2015-07-14 12:00:00', 0, 0, 0),
(4, 'Evento Final', 'El evento final del día.', '', '', 0, '', '2015-07-16', '00:00:00', '2015-07-16', '00:00:00', '00:00:00', '00:00:00', 'Calle Quintero, Altamira, Tamaulipas, México', 22.39177, -97.937412, 1, '2015-07-15 09:00:00', 0, 0, 0),
(5, 'Titulo', 'Descripcion', '', '', 0, '', '2015-07-17', '00:00:00', '2015-07-17', '00:00:00', '00:00:00', '00:00:00', 'Altamira, México', 22.4077462, -97.9211555, 1, '2015-07-17 10:52:33', 0, 0, 0),
(6, 'Evento Principal', 'El evento de inauguración que se llevara a cabo próximamente.', '', '', 0, '', '2015-07-15', '00:00:00', '2015-07-15', '00:00:00', '00:00:00', '00:00:00', 'Calle Quintero, Altamira, Tamaulipas, México', 22.39177, -97.937412, 1, '2015-07-17 11:19:26', 0, 0, 0),
(7, 'Nuevo Evento', 'Ninguna.', '', '', 0, '', '2015-07-21', '00:00:00', '2015-07-21', '00:00:00', '00:00:00', '00:00:00', 'Quintero, Zona Centro, Altamira, México', 22.3917129, -97.93645749999996, 1, '2015-07-20 09:39:29', 0, 0, 0),
(8, 'Evento Terceario', 'Descripcion', '', '', 0, '', '2015-07-21', '00:00:00', '2015-07-21', '00:00:00', '00:00:00', '00:00:00', 'Altamira, Fidel Velázquez, México', 22.4052259, -97.9272823, 1, '2015-07-20 10:45:11', 0, 0, 0),
(9, 'Evento A1', 'Es un evento especial llamado A1.', '', '', 0, '', '2015-08-17', '00:00:00', '2015-08-17', '00:00:00', '00:00:00', '00:00:00', 'Quintero, Zona Centro, Altamira, México', 22.3917129, -97.93645749999996, 1, '2015-08-17 16:22:08', 0, 0, 0),
(10, 'Evento A2', 'Este es un', '', '', 0, '', '2015-08-17', '00:00:00', '2015-08-17', '00:00:00', '00:00:00', '00:00:00', 'Querétaro, México', 20.5887932, -100.38988810000001, 1, '2015-08-17 16:36:56', 0, 0, 0),
(11, 'Este es un super evento genial magno de 1000 personas en el estadio de Altamira.', '', '', '', 0, '', '2015-09-17', '13:00:00', '2015-09-17', '15:00:00', '00:00:00', '00:00:00', 'Estado Estudiantes, Altamira, Tamaulipas', 22.401799, -97.910687, 1, '2015-08-17 16:38:19', 0, 0, 0),
(12, 'Evento A1', 'Es un evento especial llamado A1.', '', '', 0, '', '2015-08-17', '00:00:00', '2015-08-17', '00:00:00', '00:00:00', '00:00:00', 'Quintero, Zona Centro, Altamira, México', 22.3917129, -97.93645749999996, 1, '2015-08-17 17:16:52', 0, 0, 0),
(14, 'El titulo', '', '', '', 1, '', '2015-09-29', '12:00:00', '2015-09-29', '14:00:00', '12:15:00', '12:45:00', 'Venustiano Carranza 1916, Guadalupe Victoria, 89602 Altamira, Tamps., México', 22.407775210327625, -97.92188286781311, 1, '2015-09-28 15:30:01', 0, 0, 0),
(15, 'Nuevo evento', 'Es un evento nuevo.', 'Lic. Jose', 'Uniforme', 0, '', '2015-09-29', '16:00:00', '2015-09-29', '17:00:00', '16:10:00', '16:50:00', 'Salon verde', 22.41505, -97.94162399999999, 1, '2015-09-29 15:06:39', 0, 0, 0),
(16, 'nUEVO EVENTO', 'NADA', 'NADIE', 'UNIFORME', 0, '', '2015-09-30', '14:00:00', '2015-09-30', '15:00:00', '14:15:00', '14:45:00', 'Vicente Guerrero 206, Campanario Residencial, Altamira, Tamps., México', 22.394185861359183, -97.93837308883667, 1, '2015-09-30 13:58:12', 1, 0, 0),
(17, 'Es mi evento con un titulo muy grande y extenso para ver si cabe en el reporte.', 'Es mi evento con un titulo muy grande y extenso para ver si cabe en el reporte.Es mi evento con un titulo muy grande y extenso para ver si cabe en el reporte.', 'SECRETARIA DE EDUCACION DE TAMAULIPAS, PARA LA EDUCACION DE TODO EL ESTADO', 'Uniforme', 1, 'JUANITO', '2015-09-30', '11:00:00', '2015-09-30', '12:00:00', '11:15:00', '11:45:00', 'Hipólito Cepeda 302, Potosina, 89603 Altamira, Tamps., México', 22.394245379652354, -97.92837381362915, 1, '2015-10-01 10:27:05', 0, 0, 0),
(18, 'QUINTO EVENTO', 'ES UN EVENTO DE PRUEBA PARA REVISAR EL ACOMODO DEL TEXTO', 'EL SECRETARIO DE OBRAS PUBLICAS', 'FORMAL', 0, '', '2015-07-15', '13:00:00', '2015-07-15', '14:00:00', '13:10:00', '13:45:00', 'Altamira, México', 22.4077462, -97.9211555, 1, '2015-10-02 12:34:47', 0, 0, 0),
(19, 'a', '', '', '', 0, '', '2015-10-20', '10:45:00', '2015-10-20', '12:00:00', '10:15:00', '10:45:00', 'Emiliano Zapata 1715, Independencia, 89602 Altamira, Tamps., México', 22.406108850686103, -97.9236102104187, 1, '2015-10-19 08:55:05', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos_convocados`
--

CREATE TABLE IF NOT EXISTS `grupos_convocados` (
  `id_grupo_convocado` tinyint(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(120) NOT NULL,
  PRIMARY KEY (`id_grupo_convocado`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `grupos_convocados`
--

INSERT INTO `grupos_convocados` (`id_grupo_convocado`, `nombre`) VALUES
(1, 'TODOS'),
(2, 'ALCALDE'),
(3, 'CABILDO'),
(4, 'SECRETARIOS'),
(5, 'DIRECTORES'),
(6, 'JEFES DE DEPARTAMENTOS'),
(7, 'DIF');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos_convocados_eventos`
--

CREATE TABLE IF NOT EXISTS `grupos_convocados_eventos` (
  `id_convocado_evento` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo_convocado` tinyint(11) NOT NULL,
  `id_evento` int(11) NOT NULL,
  `fecha_captura` datetime NOT NULL,
  `borrado` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_convocado_evento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Volcado de datos para la tabla `grupos_convocados_eventos`
--

INSERT INTO `grupos_convocados_eventos` (`id_convocado_evento`, `id_grupo_convocado`, `id_evento`, `fecha_captura`, `borrado`) VALUES
(1, 2, 11, '2015-09-30 11:45:58', 0),
(2, 3, 11, '2015-09-30 11:45:58', 0),
(3, 4, 11, '2015-09-30 11:46:08', 0),
(4, 2, 15, '2015-09-30 13:23:25', 0),
(5, 4, 15, '2015-09-30 13:24:49', 1),
(6, 5, 14, '2015-09-30 13:25:15', 0),
(7, 6, 14, '2015-09-30 13:25:15', 0),
(8, 2, 16, '2015-09-30 13:58:12', 0),
(9, 3, 16, '2015-09-30 13:58:12', 0),
(10, 4, 16, '2015-09-30 13:58:12', 0),
(11, 2, 17, '2015-10-01 10:27:05', 0),
(12, 3, 17, '2015-10-01 10:27:05', 0),
(13, 4, 17, '2015-10-01 10:27:05', 0),
(14, 5, 17, '2015-10-01 10:27:05', 0),
(15, 6, 17, '2015-10-02 12:31:56', 0),
(16, 2, 18, '2015-10-02 12:34:47', 0),
(17, 3, 18, '2015-10-02 12:34:47', 0),
(18, 4, 18, '2015-10-02 12:34:47', 0),
(19, 5, 18, '2015-10-02 12:34:47', 0),
(20, 6, 18, '2015-10-02 12:34:47', 0),
(21, 7, 17, '2015-10-08 15:44:24', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_vistas`
--

CREATE TABLE IF NOT EXISTS `historial_vistas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `fecha_agenda` date NOT NULL,
  `fecha_captura` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles_usuarios`
--

CREATE TABLE IF NOT EXISTS `niveles_usuarios` (
  `id_nivel_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(125) NOT NULL,
  `bloqueado` tinyint(1) NOT NULL DEFAULT '0',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_nivel_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `niveles_usuarios`
--

INSERT INTO `niveles_usuarios` (`id_nivel_usuario`, `nombre`, `bloqueado`, `borrado`) VALUES
(1, 'SUDO', 0, 0),
(2, 'Administrador', 0, 0),
(3, 'Capturista', 0, 0),
(4, 'Consultor', 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos_usuarios`
--

CREATE TABLE IF NOT EXISTS `permisos_usuarios` (
  `id_permiso_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_acceso` int(11) NOT NULL,
  `id_nivel_usuario` int(11) NOT NULL,
  `borrado` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_permiso_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `permisos_usuarios`
--

INSERT INTO `permisos_usuarios` (`id_permiso_usuario`, `id_acceso`, `id_nivel_usuario`, `borrado`) VALUES
(1, 1, 1, 0),
(2, 2, 3, 0),
(3, 10, 4, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(75) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_nivel_usuario` int(11) NOT NULL,
  `nombre` varchar(200) NOT NULL,
  `paterno` varchar(200) NOT NULL,
  `materno` varchar(200) NOT NULL,
  `id_grupo_convocado` tinyint(4) NOT NULL,
  `bloqueado` tinyint(1) NOT NULL DEFAULT '0',
  `borrado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `password`, `id_nivel_usuario`, `nombre`, `paterno`, `materno`, `id_grupo_convocado`, `bloqueado`, `borrado`) VALUES
(1, 'admin', 'qweqwe', 1, 'Juanito', 'Perez', '', 1, 0, 0),
(2, 'admin1', 'qweqwe', 3, '', '', '', 1, 0, 0),
(4, 'admin2', 'qweqwe', 4, 'Hey', '', '', 3, 0, 0);

-- --------------------------------------------------------

--
-- Estructura para la vista `dt_asistencia`
--
DROP TABLE IF EXISTS `dt_asistencia`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dt_asistencia` AS select `a`.`id_asistencia` AS `id`,`e`.`titulo` AS `evento`,concat(`u`.`nombre`,' ',`u`.`paterno`,' ',`u`.`materno`) AS `nombre`,if((`a`.`asistencia` = 1),'CONFIRMO','CANCELO') AS `asistencia`,date_format(`a`.`fecha_captura`,'%d/%m/%Y %H:%i') AS `timestamp` from ((`asistencias` `a` join `eventos` `e` on((`e`.`id_evento` = `a`.`id_evento`))) join `usuarios` `u` on((`u`.`id_usuario` = `a`.`id_usuario`)));

-- --------------------------------------------------------

--
-- Estructura para la vista `dt_historial`
--
DROP TABLE IF EXISTS `dt_historial`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `dt_historial` AS select `hv`.`id` AS `id`,concat(`u`.`nombre`,' ',`u`.`paterno`,' ',`u`.`materno`) AS `nombre`,date_format(`hv`.`fecha_agenda`,'%d/%m/%Y') AS `agenda`,date_format(`hv`.`fecha_captura`,'%d/%m/%Y %H:%i') AS `timestamp` from (`historial_vistas` `hv` join `usuarios` `u` on((`u`.`id_usuario` = `hv`.`id_usuario`))) order by `hv`.`fecha_captura` desc;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
